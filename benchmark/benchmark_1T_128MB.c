#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
// #include <x86intrin.h>
#include <emmintrin.h>
#include <immintrin.h>
#include <stdint.h>

#define N 32*1024*1024

int main(void)
{
    uint32_t * A;
    uint32_t * B;
    uint32_t * C;
    
    A = (uint32_t *) malloc (N * sizeof(uint32_t));
    B = (uint32_t *) malloc (N * sizeof(uint32_t));
    C = (uint32_t *) malloc (N * sizeof(uint32_t));

    // #pragma clang loop unroll(disable) vectorize(disable) interleave(disable)
    // for(int i=0; i<N; i++) {
        // B[i] = i;
        // C[i] = i*2;
    // }
    
    // #pragma clang loop unroll(disable) vectorize(disable) interleave(disable)
    // for(int i=0; i<N; i++) {
        // _mm_clflush(&B[i]);
        // _mm_clflush(&C[i]);
    // }
    // _mm_mfence();

    //THIS IS WHAT WE WANT TO MEASURE
    for(int i=0; i<N; i++) {
        A[i] = B[i] + C[i];
    }

    uint32_t result = (A[0] + A[N/2] + A[N-1]);
    printf("Result = %d\n", result);
    
    free(A);
    free(B);
    free(C);
    
    
    return (result);
}
