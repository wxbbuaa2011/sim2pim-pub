#rvu per cores for MT = 32
	.text
	.file	"../../benchmark/compiled/IR/tempFile_o3_16.ll"
	.globl	vecsum
	.p2align	4, 0x90
	.type	vecsum,@function
vecsum:                                 # @vecsum
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	xorl	%ecx, %ecx              # encoding: [0x31,0xc9]
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, B(,%rcx,4)        # encoding: [0x89,0x0c,0x8d,A,A,A,A]
                                        #   fixup A - offset: 3, value: B, kind: reloc_signed_4byte
	movl	%eax, C(,%rcx,4)        # encoding: [0x89,0x04,0x8d,A,A,A,A]
                                        #   fixup A - offset: 3, value: C, kind: reloc_signed_4byte
	incq	%rcx                    # encoding: [0x48,0xff,0xc1]
	addl	$2, %eax                # encoding: [0x83,0xc0,0x02]
	cmpq	$2097152, %rcx          # encoding: [0x48,0x81,0xf9,0x00,0x00,0x20,0x00]
                                        # imm = 0x200000
	jne	.LBB0_1                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_1-1, kind: FK_PCRel_1
# BB#2:                                 # %.preheader.preheader
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	clflush	B(%rax)                 # encoding: [0x0f,0xae,0xb8,A,A,A,A]
                                        #   fixup A - offset: 3, value: B, kind: reloc_signed_4byte
	clflush	C(%rax)                 # encoding: [0x0f,0xae,0xb8,A,A,A,A]
                                        #   fixup A - offset: 3, value: C, kind: reloc_signed_4byte
	addq	$4, %rax                # encoding: [0x48,0x83,0xc0,0x04]
	cmpq	$8388608, %rax          # encoding: [0x48,0x3d,0x00,0x00,0x80,0x00]
                                        # imm = 0x800000
	jne	.LBB0_3                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_3-1, kind: FK_PCRel_1
# BB#4:                                 # %min.iters.checked
	mfence                          # encoding: [0x0f,0xae,0xf0]
	movq	$-8388608, %rax         # encoding: [0x48,0xc7,0xc0,0x00,0x00,0x80,0xff]
                                        # imm = 0xFF800000
	.p2align	4, 0x90
.LBB0_5:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	vmovdqa32	C+8388608(%rax), %zmm0 # encoding: [0x62,0xf1,0x7d,0x48,0x6f,0x80,A,A,A,A]
                                        #   fixup A - offset: 6, value: C+8388608, kind: reloc_signed_4byte
	vmovdqa32	C+8388672(%rax), %zmm1 # encoding: [0x62,0xf1,0x7d,0x48,0x6f,0x88,A,A,A,A]
                                        #   fixup A - offset: 6, value: C+8388672, kind: reloc_signed_4byte
	vmovdqa32	C+8388736(%rax), %zmm2 # encoding: [0x62,0xf1,0x7d,0x48,0x6f,0x90,A,A,A,A]
                                        #   fixup A - offset: 6, value: C+8388736, kind: reloc_signed_4byte
	vmovdqa32	C+8388800(%rax), %zmm3 # encoding: [0x62,0xf1,0x7d,0x48,0x6f,0x98,A,A,A,A]
                                        #   fixup A - offset: 6, value: C+8388800, kind: reloc_signed_4byte
	vpaddd	B+8388608(%rax), %zmm0, %zmm0 # encoding: [0x62,0xf1,0x7d,0x48,0xfe,0x80,A,A,A,A]
                                        #   fixup A - offset: 6, value: B+8388608, kind: reloc_signed_4byte
	vpaddd	B+8388672(%rax), %zmm1, %zmm1 # encoding: [0x62,0xf1,0x75,0x48,0xfe,0x88,A,A,A,A]
                                        #   fixup A - offset: 6, value: B+8388672, kind: reloc_signed_4byte
	vpaddd	B+8388736(%rax), %zmm2, %zmm2 # encoding: [0x62,0xf1,0x6d,0x48,0xfe,0x90,A,A,A,A]
                                        #   fixup A - offset: 6, value: B+8388736, kind: reloc_signed_4byte
	vpaddd	B+8388800(%rax), %zmm3, %zmm3 # encoding: [0x62,0xf1,0x65,0x48,0xfe,0x98,A,A,A,A]
                                        #   fixup A - offset: 6, value: B+8388800, kind: reloc_signed_4byte
