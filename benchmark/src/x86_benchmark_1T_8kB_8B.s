#rvu per cores for MT = 32
	.text
	.file	"../../benchmark/compiled/IR/tempFile_o3_2.ll"
	.globl	vecsum
	.p2align	4, 0x90
	.type	vecsum,@function
vecsum:                                 # @vecsum
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movq	$-8192, %rax            # encoding: [0x48,0xc7,0xc0,0x00,0xe0,0xff,0xff]
                                        # imm = 0xE000
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	vmovq	B+8192(%rax), %xmm0     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8192, kind: reloc_signed_4byte
                                        # xmm0 = mem[0],zero
	vmovq	B+8200(%rax), %xmm1     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x88,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8200, kind: reloc_signed_4byte
                                        # xmm1 = mem[0],zero
	vmovq	B+8208(%rax), %xmm2     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x90,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8208, kind: reloc_signed_4byte
                                        # xmm2 = mem[0],zero
	vmovq	B+8216(%rax), %xmm3     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x98,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8216, kind: reloc_signed_4byte
                                        # xmm3 = mem[0],zero
	vmovq	C+8192(%rax), %xmm4     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xa0,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8192, kind: reloc_signed_4byte
                                        # xmm4 = mem[0],zero
	vmovq	C+8200(%rax), %xmm5     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xa8,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8200, kind: reloc_signed_4byte
                                        # xmm5 = mem[0],zero
	vmovq	C+8208(%rax), %xmm6     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xb0,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8208, kind: reloc_signed_4byte
                                        # xmm6 = mem[0],zero
	vmovq	C+8216(%rax), %xmm7     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xb8,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8216, kind: reloc_signed_4byte
                                        # xmm7 = mem[0],zero
	vpaddd	%xmm0, %xmm4, %xmm0     # encoding: [0xc5,0xd9,0xfe,0xc0]
	vmovq	%xmm0, A+8192(%rax)     # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8192, kind: reloc_signed_4byte
	vpaddd	%xmm1, %xmm5, %xmm0     # encoding: [0xc5,0xd1,0xfe,0xc1]
	vmovq	%xmm0, A+8200(%rax)     # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8200, kind: reloc_signed_4byte
	vpaddd	%xmm2, %xmm6, %xmm0     # encoding: [0xc5,0xc9,0xfe,0xc2]
	vmovq	%xmm0, A+8208(%rax)     # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8208, kind: reloc_signed_4byte
	vpaddd	%xmm3, %xmm7, %xmm0     # encoding: [0xc5,0xc1,0xfe,0xc3]
	vmovq	%xmm0, A+8216(%rax)     # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8216, kind: reloc_signed_4byte
	vmovq	B+8224(%rax), %xmm0     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8224, kind: reloc_signed_4byte
                                        # xmm0 = mem[0],zero
	vmovq	B+8232(%rax), %xmm1     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x88,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8232, kind: reloc_signed_4byte
                                        # xmm1 = mem[0],zero
	vmovq	B+8240(%rax), %xmm2     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x90,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8240, kind: reloc_signed_4byte
                                        # xmm2 = mem[0],zero
	vmovq	B+8248(%rax), %xmm3     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x98,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8248, kind: reloc_signed_4byte
                                        # xmm3 = mem[0],zero
	vmovq	C+8224(%rax), %xmm4     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xa0,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8224, kind: reloc_signed_4byte
                                        # xmm4 = mem[0],zero
	vmovq	C+8232(%rax), %xmm5     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xa8,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8232, kind: reloc_signed_4byte
                                        # xmm5 = mem[0],zero
	vmovq	C+8240(%rax), %xmm6     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xb0,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8240, kind: reloc_signed_4byte
                                        # xmm6 = mem[0],zero
	vmovq	C+8248(%rax), %xmm7     # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xb8,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8248, kind: reloc_signed_4byte
                                        # xmm7 = mem[0],zero
	vpaddd	%xmm0, %xmm4, %xmm0     # encoding: [0xc5,0xd9,0xfe,0xc0]
	vmovq	%xmm0, A+8224(%rax)     # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8224, kind: reloc_signed_4byte
	vpaddd	%xmm1, %xmm5, %xmm0     # encoding: [0xc5,0xd1,0xfe,0xc1]
	vmovq	%xmm0, A+8232(%rax)     # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8232, kind: reloc_signed_4byte
	vpaddd	%xmm2, %xmm6, %xmm0     # encoding: [0xc5,0xc9,0xfe,0xc2]
	vmovq	%xmm0, A+8240(%rax)     # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8240, kind: reloc_signed_4byte
	vpaddd	%xmm3, %xmm7, %xmm0     # encoding: [0xc5,0xc1,0xfe,0xc3]
	vmovq	%xmm0, A+8248(%rax)     # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8248, kind: reloc_signed_4byte
	addq	$64, %rax               # encoding: [0x48,0x83,0xc0,0x40]
	jne	.LBB0_1                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_1-1, kind: FK_PCRel_1
# BB#2:                                 # %middle.block
	pushq	%rax                    # encoding: [0x50]
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	A+4096(%rip), %esi      # encoding: [0x8b,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %esi           # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %esi      # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	movl	$.L.str, %edi           # encoding: [0xbf,A,A,A,A]
                                        #   fixup A - offset: 1, value: .L.str, kind: FK_Data_4
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	callq	printf                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: printf-4, kind: FK_PCRel_4
	movl	A+4096(%rip), %eax      # encoding: [0x8b,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %eax           # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %eax      # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	popq	%rcx                    # encoding: [0x59]
	retq                            # encoding: [0xc3]
.Lfunc_end0:
	.size	vecsum, .Lfunc_end0-vecsum
	.cfi_endproc

	.type	B,@object               # @B
	.comm	B,8192,64
	.type	C,@object               # @C
	.comm	C,8192,64
	.type	A,@object               # @A
	.comm	A,8192,64
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Result = %d\n"
	.size	.L.str, 13

	.globl	PIM_ADDR
	.data
	.align 16
	.type	PIM_ADDR,@object      # @PIM_ADDR
	.size	PIM_ADDR, 512


	.globl	PIM_LS
	.data
	.align 16
	.type	PIM_LS,@object      # @PIM_LS
	.size	PIM_LS, 512


	.section	".note.GNU-stack","",@progbits
