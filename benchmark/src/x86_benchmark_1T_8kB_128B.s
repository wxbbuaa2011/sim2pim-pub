#rvu per cores for MT = 32
	.text
	.file	"../../benchmark/compiled/IR/tempFile_o3_32.ll"
	.globl	vecsum
	.p2align	4, 0x90
	.type	vecsum,@function
vecsum:                                 # @vecsum
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movq	$-8192, %rax            # encoding: [0x48,0xc7,0xc0,0x00,0xe0,0xff,0xff]
                                        # imm = 0xE000
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
    ###	PIM_128B_LOAD_DWORD	B+8192(%rax), %RVU_0_R1024b_0 # encoding: [0x61,0x00,0x45,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8192(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8192, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	B+8320(%rax), %RVU_0_R1024b_1 # encoding: [0x61,0x00,0x45,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8320(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8320, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	B+8448(%rax), %RVU_0_R1024b_2 # encoding: [0x61,0x00,0x45,0x00,0x00,0x40,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8448(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8448, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	B+8576(%rax), %RVU_0_R1024b_3 # encoding: [0x61,0x00,0x45,0x00,0x00,0x60,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8576(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8576, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	C+8192(%rax), %RVU_0_R1024b_4 # encoding: [0x61,0x00,0x45,0x00,0x00,0x80,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a000080000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8192(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8192, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	C+8320(%rax), %RVU_0_R1024b_5 # encoding: [0x61,0x00,0x45,0x00,0x00,0xa0,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a0000a0000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8320(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8320, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	C+8448(%rax), %RVU_0_R1024b_6 # encoding: [0x61,0x00,0x45,0x00,0x00,0xc0,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a0000c0000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8448(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8448, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	C+8576(%rax), %RVU_0_R1024b_7 # encoding: [0x61,0x00,0x45,0x00,0x00,0xe0,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a0000e0000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8576(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8576, kind: reloc_signed_4byte
    ###	PIM_128B_VADD_DWORD	%RVU_0_R1024b_4, %RVU_0_R1024b_0, %RVU_0_R1024b_0 # encoding: [0x61,0x02,0x45,0x00,0x00,0x00,0x00,0x10,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x022a000000001000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_128B_VADD_DWORD	%RVU_0_R1024b_5, %RVU_0_R1024b_1, %RVU_0_R1024b_1 # encoding: [0x61,0x02,0x45,0x00,0x00,0x20,0x00,0x14,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x022a000020001400 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_128B_VADD_DWORD	%RVU_0_R1024b_6, %RVU_0_R1024b_2, %RVU_0_R1024b_2 # encoding: [0x61,0x02,0x45,0x00,0x00,0x40,0x00,0x18,0x00,0x01,0x00]
    pushq %rax
    mfence
    movq	$0x022a000040001800 , %rax
    movq	%rax, %xmm0
    movq	$0x0100000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_128B_VADD_DWORD	%RVU_0_R1024b_7, %RVU_0_R1024b_3, %RVU_0_R1024b_3 # encoding: [0x61,0x02,0x45,0x00,0x00,0x60,0x00,0x1c,0x00,0x01,0x80]
    pushq %rax
    mfence
    movq	$0x022a000060001c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0180000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_128B_STORE_DWORD	%RVU_0_R1024b_0, A+8192(%rax) # encoding: [0x61,0x01,0x45,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x012a000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8192(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8192, kind: reloc_signed_4byte
    ###	PIM_128B_STORE_DWORD	%RVU_0_R1024b_1, A+8320(%rax) # encoding: [0x61,0x01,0x45,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x012a000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8320(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8320, kind: reloc_signed_4byte
    ###	PIM_128B_STORE_DWORD	%RVU_0_R1024b_2, A+8448(%rax) # encoding: [0x61,0x01,0x45,0x00,0x00,0x40,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x012a000040000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8448(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8448, kind: reloc_signed_4byte
    ###	PIM_128B_STORE_DWORD	%RVU_0_R1024b_3, A+8576(%rax) # encoding: [0x61,0x01,0x45,0x00,0x00,0x60,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x012a000060000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8576(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8576, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	B+8704(%rax), %RVU_0_R1024b_0 # encoding: [0x61,0x00,0x45,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8704(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8704, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	B+8832(%rax), %RVU_0_R1024b_1 # encoding: [0x61,0x00,0x45,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8832(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8832, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	B+8960(%rax), %RVU_0_R1024b_2 # encoding: [0x61,0x00,0x45,0x00,0x00,0x40,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8960(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8960, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	B+9088(%rax), %RVU_0_R1024b_3 # encoding: [0x61,0x00,0x45,0x00,0x00,0x60,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+9088(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+9088, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	C+8704(%rax), %RVU_0_R1024b_4 # encoding: [0x61,0x00,0x45,0x00,0x00,0x80,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a000080000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8704(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8704, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	C+8832(%rax), %RVU_0_R1024b_5 # encoding: [0x61,0x00,0x45,0x00,0x00,0xa0,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a0000a0000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8832(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8832, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	C+8960(%rax), %RVU_0_R1024b_6 # encoding: [0x61,0x00,0x45,0x00,0x00,0xc0,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a0000c0000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8960(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8960, kind: reloc_signed_4byte
    ###	PIM_128B_LOAD_DWORD	C+9088(%rax), %RVU_0_R1024b_7 # encoding: [0x61,0x00,0x45,0x00,0x00,0xe0,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x002a0000e0000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+9088(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+9088, kind: reloc_signed_4byte
    ###	PIM_128B_VADD_DWORD	%RVU_0_R1024b_4, %RVU_0_R1024b_0, %RVU_0_R1024b_0 # encoding: [0x61,0x02,0x45,0x00,0x00,0x00,0x00,0x10,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x022a000000001000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_128B_VADD_DWORD	%RVU_0_R1024b_5, %RVU_0_R1024b_1, %RVU_0_R1024b_1 # encoding: [0x61,0x02,0x45,0x00,0x00,0x20,0x00,0x14,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x022a000020001400 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_128B_VADD_DWORD	%RVU_0_R1024b_6, %RVU_0_R1024b_2, %RVU_0_R1024b_2 # encoding: [0x61,0x02,0x45,0x00,0x00,0x40,0x00,0x18,0x00,0x01,0x00]
    pushq %rax
    mfence
    movq	$0x022a000040001800 , %rax
    movq	%rax, %xmm0
    movq	$0x0100000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_128B_VADD_DWORD	%RVU_0_R1024b_7, %RVU_0_R1024b_3, %RVU_0_R1024b_3 # encoding: [0x61,0x02,0x45,0x00,0x00,0x60,0x00,0x1c,0x00,0x01,0x80]
    pushq %rax
    mfence
    movq	$0x022a000060001c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0180000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_128B_STORE_DWORD	%RVU_0_R1024b_0, A+8704(%rax) # encoding: [0x61,0x01,0x45,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x012a000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8704(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8704, kind: reloc_signed_4byte
    ###	PIM_128B_STORE_DWORD	%RVU_0_R1024b_1, A+8832(%rax) # encoding: [0x61,0x01,0x45,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x012a000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8832(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8832, kind: reloc_signed_4byte
    ###	PIM_128B_STORE_DWORD	%RVU_0_R1024b_2, A+8960(%rax) # encoding: [0x61,0x01,0x45,0x00,0x00,0x40,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x012a000040000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8960(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8960, kind: reloc_signed_4byte
    ###	PIM_128B_STORE_DWORD	%RVU_0_R1024b_3, A+9088(%rax) # encoding: [0x61,0x01,0x45,0x00,0x00,0x60,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x012a000060000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+9088(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+9088, kind: reloc_signed_4byte
	addq	$1024, %rax             # encoding: [0x48,0x05,0x00,0x04,0x00,0x00]
                                        # imm = 0x400
	jne	.LBB0_1                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_1-1, kind: FK_PCRel_1
# BB#2:                                 # %middle.block
	pushq	%rax                    # encoding: [0x50]
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	A+4096(%rip), %esi      # encoding: [0x8b,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %esi           # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %esi      # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	movl	$.L.str, %edi           # encoding: [0xbf,A,A,A,A]
                                        #   fixup A - offset: 1, value: .L.str, kind: FK_Data_4
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	callq	printf                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: printf-4, kind: FK_PCRel_4
	movl	A+4096(%rip), %eax      # encoding: [0x8b,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %eax           # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %eax      # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	popq	%rcx                    # encoding: [0x59]
	retq                            # encoding: [0xc3]
.Lfunc_end0:
	.size	vecsum, .Lfunc_end0-vecsum
	.cfi_endproc

	.type	B,@object               # @B
	.comm	B,8192,64
	.type	C,@object               # @C
	.comm	C,8192,64
	.type	A,@object               # @A
	.comm	A,8192,64
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Result = %d\n"
	.size	.L.str, 13

	.globl	PIM_ADDR
	.data
	.align 16
	.type	PIM_ADDR,@object      # @PIM_ADDR
	.size	PIM_ADDR, 512


	.globl	PIM_LS
	.data
	.align 16
	.type	PIM_LS,@object      # @PIM_LS
	.size	PIM_LS, 512


	.section	".note.GNU-stack","",@progbits
