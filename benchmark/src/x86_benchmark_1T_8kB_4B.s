#rvu per cores for MT = 32
	.text
	.file	"../../benchmark/compiled/IR/tempFile_o3_1.ll"
	.globl	vecsum
	.p2align	4, 0x90
	.type	vecsum,@function
vecsum:                                 # @vecsum
	.cfi_startproc
# BB#0:
	movq	$-8192, %rax            # encoding: [0x48,0xc7,0xc0,0x00,0xe0,0xff,0xff]
                                        # imm = 0xE000
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	C+8192(%rax), %ecx      # encoding: [0x8b,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: C+8192, kind: reloc_signed_4byte_relax
	addl	B+8192(%rax), %ecx      # encoding: [0x03,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: B+8192, kind: reloc_signed_4byte
	movl	%ecx, A+8192(%rax)      # encoding: [0x89,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: A+8192, kind: reloc_signed_4byte
	movl	C+8196(%rax), %ecx      # encoding: [0x8b,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: C+8196, kind: reloc_signed_4byte_relax
	addl	B+8196(%rax), %ecx      # encoding: [0x03,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: B+8196, kind: reloc_signed_4byte
	movl	%ecx, A+8196(%rax)      # encoding: [0x89,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: A+8196, kind: reloc_signed_4byte
	movl	C+8200(%rax), %ecx      # encoding: [0x8b,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: C+8200, kind: reloc_signed_4byte_relax
	addl	B+8200(%rax), %ecx      # encoding: [0x03,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: B+8200, kind: reloc_signed_4byte
	movl	%ecx, A+8200(%rax)      # encoding: [0x89,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: A+8200, kind: reloc_signed_4byte
	movl	C+8204(%rax), %ecx      # encoding: [0x8b,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: C+8204, kind: reloc_signed_4byte_relax
	addl	B+8204(%rax), %ecx      # encoding: [0x03,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: B+8204, kind: reloc_signed_4byte
	movl	%ecx, A+8204(%rax)      # encoding: [0x89,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: A+8204, kind: reloc_signed_4byte
	movl	C+8208(%rax), %ecx      # encoding: [0x8b,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: C+8208, kind: reloc_signed_4byte_relax
	addl	B+8208(%rax), %ecx      # encoding: [0x03,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: B+8208, kind: reloc_signed_4byte
	movl	%ecx, A+8208(%rax)      # encoding: [0x89,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: A+8208, kind: reloc_signed_4byte
	movl	C+8212(%rax), %ecx      # encoding: [0x8b,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: C+8212, kind: reloc_signed_4byte_relax
	addl	B+8212(%rax), %ecx      # encoding: [0x03,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: B+8212, kind: reloc_signed_4byte
	movl	%ecx, A+8212(%rax)      # encoding: [0x89,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: A+8212, kind: reloc_signed_4byte
	movl	C+8216(%rax), %ecx      # encoding: [0x8b,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: C+8216, kind: reloc_signed_4byte_relax
	addl	B+8216(%rax), %ecx      # encoding: [0x03,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: B+8216, kind: reloc_signed_4byte
	movl	%ecx, A+8216(%rax)      # encoding: [0x89,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: A+8216, kind: reloc_signed_4byte
	movl	C+8220(%rax), %ecx      # encoding: [0x8b,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: C+8220, kind: reloc_signed_4byte_relax
	addl	B+8220(%rax), %ecx      # encoding: [0x03,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: B+8220, kind: reloc_signed_4byte
	movl	%ecx, A+8220(%rax)      # encoding: [0x89,0x88,A,A,A,A]
                                        #   fixup A - offset: 2, value: A+8220, kind: reloc_signed_4byte
	addq	$32, %rax               # encoding: [0x48,0x83,0xc0,0x20]
	jne	.LBB0_1                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_1-1, kind: FK_PCRel_1
# BB#2:
	pushq	%rax                    # encoding: [0x50]
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	A+4096(%rip), %esi      # encoding: [0x8b,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %esi           # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %esi      # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	movl	$.L.str, %edi           # encoding: [0xbf,A,A,A,A]
                                        #   fixup A - offset: 1, value: .L.str, kind: FK_Data_4
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	callq	printf                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: printf-4, kind: FK_PCRel_4
	movl	A+4096(%rip), %eax      # encoding: [0x8b,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %eax           # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %eax      # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	popq	%rcx                    # encoding: [0x59]
	retq                            # encoding: [0xc3]
.Lfunc_end0:
	.size	vecsum, .Lfunc_end0-vecsum
	.cfi_endproc

	.type	B,@object               # @B
	.comm	B,8192,64
	.type	C,@object               # @C
	.comm	C,8192,64
	.type	A,@object               # @A
	.comm	A,8192,64
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Result = %d\n"
	.size	.L.str, 13

	.globl	PIM_ADDR
	.data
	.align 16
	.type	PIM_ADDR,@object      # @PIM_ADDR
	.size	PIM_ADDR, 512


	.globl	PIM_LS
	.data
	.align 16
	.type	PIM_LS,@object      # @PIM_LS
	.size	PIM_LS, 512


	.section	".note.GNU-stack","",@progbits
