#rvu per cores for MT = 32
	.text
	.file	"../../benchmark/compiled/IR/tempFile_o3_1.ll"
	.globl	vecsum
	.p2align	4, 0x90
	.type	vecsum,@function
vecsum:                                 # @vecsum
	.cfi_startproc
# BB#0:
	pushq	%rbp                    # encoding: [0x55]
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15                    # encoding: [0x41,0x57]
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14                    # encoding: [0x41,0x56]
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx                    # encoding: [0x53]
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax                    # encoding: [0x50]
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	$134217728, %edi        # encoding: [0xbf,0x00,0x00,0x00,0x08]
                                        # imm = 0x8000000
	callq	malloc                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: malloc-4, kind: FK_PCRel_4
	movq	%rax, %r14              # encoding: [0x49,0x89,0xc6]
	movl	$134217728, %edi        # encoding: [0xbf,0x00,0x00,0x00,0x08]
                                        # imm = 0x8000000
	callq	malloc                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: malloc-4, kind: FK_PCRel_4
	movq	%rax, %r15              # encoding: [0x49,0x89,0xc7]
	movl	$134217728, %edi        # encoding: [0xbf,0x00,0x00,0x00,0x08]
                                        # imm = 0x8000000
	callq	malloc                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: malloc-4, kind: FK_PCRel_4
	movq	%rax, %rbx              # encoding: [0x48,0x89,0xc3]
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rax), %ecx       # encoding: [0x8b,0x0c,0x03]
	addl	(%r15,%rax), %ecx       # encoding: [0x41,0x03,0x0c,0x07]
	movl	%ecx, (%r14,%rax)       # encoding: [0x41,0x89,0x0c,0x06]
	movl	4(%rbx,%rax), %ecx      # encoding: [0x8b,0x4c,0x03,0x04]
	addl	4(%r15,%rax), %ecx      # encoding: [0x41,0x03,0x4c,0x07,0x04]
	movl	%ecx, 4(%r14,%rax)      # encoding: [0x41,0x89,0x4c,0x06,0x04]
	movl	8(%rbx,%rax), %ecx      # encoding: [0x8b,0x4c,0x03,0x08]
	addl	8(%r15,%rax), %ecx      # encoding: [0x41,0x03,0x4c,0x07,0x08]
	movl	%ecx, 8(%r14,%rax)      # encoding: [0x41,0x89,0x4c,0x06,0x08]
	movl	12(%rbx,%rax), %ecx     # encoding: [0x8b,0x4c,0x03,0x0c]
	addl	12(%r15,%rax), %ecx     # encoding: [0x41,0x03,0x4c,0x07,0x0c]
	movl	%ecx, 12(%r14,%rax)     # encoding: [0x41,0x89,0x4c,0x06,0x0c]
	movl	16(%rbx,%rax), %ecx     # encoding: [0x8b,0x4c,0x03,0x10]
	addl	16(%r15,%rax), %ecx     # encoding: [0x41,0x03,0x4c,0x07,0x10]
	movl	%ecx, 16(%r14,%rax)     # encoding: [0x41,0x89,0x4c,0x06,0x10]
	movl	20(%rbx,%rax), %ecx     # encoding: [0x8b,0x4c,0x03,0x14]
	addl	20(%r15,%rax), %ecx     # encoding: [0x41,0x03,0x4c,0x07,0x14]
	movl	%ecx, 20(%r14,%rax)     # encoding: [0x41,0x89,0x4c,0x06,0x14]
	movl	24(%rbx,%rax), %ecx     # encoding: [0x8b,0x4c,0x03,0x18]
	addl	24(%r15,%rax), %ecx     # encoding: [0x41,0x03,0x4c,0x07,0x18]
	movl	%ecx, 24(%r14,%rax)     # encoding: [0x41,0x89,0x4c,0x06,0x18]
	movl	28(%rbx,%rax), %ecx     # encoding: [0x8b,0x4c,0x03,0x1c]
	addl	28(%r15,%rax), %ecx     # encoding: [0x41,0x03,0x4c,0x07,0x1c]
	movl	%ecx, 28(%r14,%rax)     # encoding: [0x41,0x89,0x4c,0x06,0x1c]
	addq	$32, %rax               # encoding: [0x48,0x83,0xc0,0x20]
	cmpq	$134217728, %rax        # encoding: [0x48,0x3d,0x00,0x00,0x00,0x08]
                                        # imm = 0x8000000
	jne	.LBB0_1                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_1-1, kind: FK_PCRel_1
# BB#2:
	movl	67108864(%r14), %ebp    # encoding: [0x41,0x8b,0xae,0x00,0x00,0x00,0x04]
	addl	(%r14), %ebp            # encoding: [0x41,0x03,0x2e]
	addl	134217724(%r14), %ebp   # encoding: [0x41,0x03,0xae,0xfc,0xff,0xff,0x07]
	movl	$.L.str, %edi           # encoding: [0xbf,A,A,A,A]
                                        #   fixup A - offset: 1, value: .L.str, kind: FK_Data_4
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	movl	%ebp, %esi              # encoding: [0x89,0xee]
	callq	printf                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: printf-4, kind: FK_PCRel_4
	movq	%r14, %rdi              # encoding: [0x4c,0x89,0xf7]
	callq	free                    # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: free-4, kind: FK_PCRel_4
	movq	%r15, %rdi              # encoding: [0x4c,0x89,0xff]
	callq	free                    # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: free-4, kind: FK_PCRel_4
	movq	%rbx, %rdi              # encoding: [0x48,0x89,0xdf]
	callq	free                    # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: free-4, kind: FK_PCRel_4
	movl	%ebp, %eax              # encoding: [0x89,0xe8]
	addq	$8, %rsp                # encoding: [0x48,0x83,0xc4,0x08]
	popq	%rbx                    # encoding: [0x5b]
	popq	%r14                    # encoding: [0x41,0x5e]
	popq	%r15                    # encoding: [0x41,0x5f]
	popq	%rbp                    # encoding: [0x5d]
	retq                            # encoding: [0xc3]
.Lfunc_end0:
	.size	vecsum, .Lfunc_end0-vecsum
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Result = %d\n"
	.size	.L.str, 13

	.globl	PIM_ADDR
	.data
	.align 16
	.type	PIM_ADDR,@object      # @PIM_ADDR
	.size	PIM_ADDR, 512


	.globl	PIM_LS
	.data
	.align 16
	.type	PIM_LS,@object      # @PIM_LS
	.size	PIM_LS, 512


	.section	".note.GNU-stack","",@progbits
