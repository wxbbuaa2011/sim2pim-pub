#rvu per cores for MT = 32
	.text
	.file	"../../benchmark/compiled/IR/tempFile_o3_64.ll"
	.globl	vecsum
	.p2align	4, 0x90
	.type	vecsum,@function
vecsum:                                 # @vecsum
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	xorl	%ecx, %ecx              # encoding: [0x31,0xc9]
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, B(,%rcx,4)        # encoding: [0x89,0x0c,0x8d,A,A,A,A]
                                        #   fixup A - offset: 3, value: B, kind: reloc_signed_4byte
	movl	%eax, C(,%rcx,4)        # encoding: [0x89,0x04,0x8d,A,A,A,A]
                                        #   fixup A - offset: 3, value: C, kind: reloc_signed_4byte
	incq	%rcx                    # encoding: [0x48,0xff,0xc1]
	addl	$2, %eax                # encoding: [0x83,0xc0,0x02]
	cmpq	$2097152, %rcx          # encoding: [0x48,0x81,0xf9,0x00,0x00,0x20,0x00]
                                        # imm = 0x200000
	jne	.LBB0_1                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_1-1, kind: FK_PCRel_1
# BB#2:                                 # %.preheader.preheader
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	clflush	B(%rax)                 # encoding: [0x0f,0xae,0xb8,A,A,A,A]
                                        #   fixup A - offset: 3, value: B, kind: reloc_signed_4byte
	clflush	C(%rax)                 # encoding: [0x0f,0xae,0xb8,A,A,A,A]
                                        #   fixup A - offset: 3, value: C, kind: reloc_signed_4byte
	addq	$4, %rax                # encoding: [0x48,0x83,0xc0,0x04]
	cmpq	$8388608, %rax          # encoding: [0x48,0x3d,0x00,0x00,0x80,0x00]
                                        # imm = 0x800000
	jne	.LBB0_3                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_3-1, kind: FK_PCRel_1
# BB#4:                                 # %min.iters.checked
	mfence                          # encoding: [0x0f,0xae,0xf0]
	movq	$-8388608, %rax         # encoding: [0x48,0xc7,0xc0,0x00,0x00,0x80,0xff]
                                        # imm = 0xFF800000
	.p2align	4, 0x90
.LBB0_5:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
    ###	PIM_256B_LOAD_DWORD	B+8388608(%rax), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8388608(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8388608, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	B+8388864(%rax), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8388864(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8388864, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	C+8388608(%rax), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x40,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8388608(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8388608, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	C+8388864(%rax), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x60,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8388864(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8388864, kind: reloc_signed_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00, %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+8388608(%rax) # encoding: [0x61,0x01,0x46,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8388608(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8388608, kind: reloc_signed_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+8388864(%rax) # encoding: [0x61,0x01,0x46,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8388864(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8388864, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	B+8389120(%rax), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8389120(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8389120, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	B+8389376(%rax), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8389376(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8389376, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	C+8389120(%rax), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x40,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8389120(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8389120, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	C+8389376(%rax), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x60,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8389376(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8389376, kind: reloc_signed_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00, %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+8389120(%rax) # encoding: [0x61,0x01,0x46,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8389120(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8389120, kind: reloc_signed_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+8389376(%rax) # encoding: [0x61,0x01,0x46,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8389376(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8389376, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	B+8389632(%rax), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8389632(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8389632, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	B+8389888(%rax), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8389888(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8389888, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	C+8389632(%rax), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x40,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8389632(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8389632, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	C+8389888(%rax), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x60,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8389888(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8389888, kind: reloc_signed_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00, %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+8389632(%rax) # encoding: [0x61,0x01,0x46,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8389632(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8389632, kind: reloc_signed_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+8389888(%rax) # encoding: [0x61,0x01,0x46,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8389888(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8389888, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	B+8390144(%rax), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8390144(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8390144, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	B+8390400(%rax), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq B+8390400(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B+8390400, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	C+8390144(%rax), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x40,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8390144(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8390144, kind: reloc_signed_4byte
    ###	PIM_256B_LOAD_DWORD	C+8390400(%rax), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x60,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq C+8390400(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C+8390400, kind: reloc_signed_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00, %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+8390144(%rax) # encoding: [0x61,0x01,0x46,0x00,0x00,0x00,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8390144(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8390144, kind: reloc_signed_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+8390400(%rax) # encoding: [0x61,0x01,0x46,0x00,0x00,0x20,0x00,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq A+8390400(%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A+8390400, kind: reloc_signed_4byte
	addq	$2048, %rax             # encoding: [0x48,0x05,0x00,0x08,0x00,0x00]
                                        # imm = 0x800
	jne	.LBB0_5                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_5-1, kind: FK_PCRel_1
# BB#6:                                 # %middle.block
	pushq	%rax                    # encoding: [0x50]
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	A+4194304(%rip), %esi   # encoding: [0x8b,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4194304)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %esi           # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8388604(%rip), %esi   # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8388604)-4, kind: reloc_riprel_4byte
	movl	$.L.str, %edi           # encoding: [0xbf,A,A,A,A]
                                        #   fixup A - offset: 1, value: .L.str, kind: FK_Data_4
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	callq	printf                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: printf-4, kind: FK_PCRel_4
	movl	A+4194304(%rip), %eax   # encoding: [0x8b,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4194304)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %eax           # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8388604(%rip), %eax   # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8388604)-4, kind: reloc_riprel_4byte
	popq	%rcx                    # encoding: [0x59]
	retq                            # encoding: [0xc3]
.Lfunc_end0:
	.size	vecsum, .Lfunc_end0-vecsum
	.cfi_endproc

	.type	B,@object               # @B
	.comm	B,8388608,64
	.type	C,@object               # @C
	.comm	C,8388608,64
	.type	A,@object               # @A
	.comm	A,8388608,64
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Result = %d\n"
	.size	.L.str, 13

	.globl	PIM_ADDR
	.data
	.align 16
	.type	PIM_ADDR,@object      # @PIM_ADDR
	.size	PIM_ADDR, 512


	.globl	PIM_LS
	.data
	.align 16
	.type	PIM_LS,@object      # @PIM_LS
	.size	PIM_LS, 512


	.section	".note.GNU-stack","",@progbits
