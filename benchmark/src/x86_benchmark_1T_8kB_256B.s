#rvu per cores for MT = 32
	.text
	.file	"../../benchmark/compiled/IR/tempFile_o3_64.ll"
	.globl	vecsum
	.p2align	4, 0x90
	.type	vecsum,@function
vecsum:                                 # @vecsum
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	pushq	%rax                    # encoding: [0x50]
.Lcfi0:
	.cfi_def_cfa_offset 16
    ###	PIM_256B_LOAD_DWORD	B(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+256(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+256(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+256)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+256(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+256(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+256)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+256(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+256(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+256)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+512(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+512(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+512)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+768(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+768(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+768)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+512(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+512(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+512)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+768(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+768(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+768)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+512(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+512(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+512)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+768(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+768(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+768)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+1024(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+1024(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+1024)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+1280(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+1280(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+1280)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+1024(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+1024(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+1024)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+1280(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+1280(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+1280)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+1024(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+1024(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+1024)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+1280(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+1280(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+1280)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+1536(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+1536(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+1536)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+1792(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+1792(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+1792)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+1536(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+1536(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+1536)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+1792(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+1792(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+1792)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+1536(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+1536(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+1536)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+1792(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+1792(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+1792)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+2048(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+2048(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+2048)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+2304(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+2304(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+2304)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+2048(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+2048(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+2048)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+2304(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+2304(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+2304)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+2048(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+2048(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+2048)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+2304(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+2304(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+2304)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+2560(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+2560(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+2560)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+2816(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+2816(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+2816)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+2560(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+2560(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+2560)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+2816(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+2816(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+2816)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+2560(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+2560(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+2560)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+2816(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+2816(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+2816)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+3072(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+3072(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+3072)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+3328(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+3328(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+3328)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+3072(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+3072(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+3072)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+3328(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+3328(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+3328)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+3072(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+3072(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+3072)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+3328(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+3328(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+3328)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+3584(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+3584(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+3584)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+3840(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+3840(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+3840)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+3584(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+3584(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+3584)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+3840(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+3840(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+3840)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+3584(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+3584(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+3584)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+3840(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+3840(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+3840)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+4096(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+4096(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+4096)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+4352(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+4352(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+4352)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+4096(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+4096(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+4096)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+4352(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+4352(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+4352)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+4096(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+4096(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+4096)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+4352(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+4352(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+4352)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+4608(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+4608(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+4608)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+4864(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+4864(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+4864)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+4608(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+4608(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+4608)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+4864(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+4864(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+4864)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+4608(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+4608(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+4608)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+4864(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+4864(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+4864)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+5120(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+5120(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+5120)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+5376(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+5376(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+5376)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+5120(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+5120(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+5120)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+5376(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+5376(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+5376)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+5120(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+5120(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+5120)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+5376(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+5376(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+5376)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+5632(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+5632(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+5632)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+5888(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+5888(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+5888)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+5632(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+5632(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+5632)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+5888(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+5888(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+5888)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+5632(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+5632(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+5632)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+5888(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+5888(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+5888)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+6144(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+6144(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+6144)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+6400(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+6400(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+6400)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+6144(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+6144(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+6144)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+6400(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+6400(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+6400)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+6144(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+6144(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+6144)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+6400(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+6400(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+6400)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+6656(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+6656(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+6656)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+6912(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+6912(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+6912)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+6656(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+6656(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+6656)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+6912(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+6912(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+6912)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+6656(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+6656(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+6656)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+6912(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+6912(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+6912)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+7168(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+7168(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+7168)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+7424(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+7424(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+7424)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+7168(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+7168(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+7168)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+7424(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+7424(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+7424)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+7168(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+7168(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+7168)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+7424(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+7424(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+7424)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+7680(%rip), %RVU_0_R2048b_0 # encoding: [0x61,0x00,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+7680(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+7680)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	B+7936(%rip), %RVU_0_R2048b_1 # encoding: [0x61,0x00,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+7936(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+7936)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+7680(%rip), %RVU_0_R2048b_2 # encoding: [0x61,0x00,0x46,0x00,0x00,0x46,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000040000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+7680(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+7680)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_LOAD_DWORD	C+7936(%rip), %RVU_0_R2048b_3 # encoding: [0x61,0x00,0x46,0x00,0x00,0x66,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0032000060000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+7936(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+7936)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_2, %RVU_0_R2048b_0, %RVU_0_R2048b_0 # encoding: [0x61,0x02,0x46,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0232000000000800 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_VADD_DWORD	%RVU_0_R2048b_3, %RVU_0_R2048b_1, %RVU_0_R2048b_1 # encoding: [0x61,0x02,0x46,0x00,0x00,0x20,0x00,0x0c,0x00,0x00,0x80]
    pushq %rax
    mfence
    movq	$0x0232000020000c00 , %rax
    movq	%rax, %xmm0
    movq	$0x0080000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_0, A+7680(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+7680(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+7680)-4, kind: reloc_riprel_4byte
    ###	PIM_256B_STORE_DWORD	%RVU_0_R2048b_1, A+7936(%rip) # encoding: [0x61,0x01,0x46,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0132000020000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+7936(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+7936)-4, kind: reloc_riprel_4byte
	movl	A+4096(%rip), %esi      # encoding: [0x8b,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %esi           # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %esi      # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	movl	$.L.str, %edi           # encoding: [0xbf,A,A,A,A]
                                        #   fixup A - offset: 1, value: .L.str, kind: FK_Data_4
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	callq	printf                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: printf-4, kind: FK_PCRel_4
	movl	A+4096(%rip), %eax      # encoding: [0x8b,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %eax           # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %eax      # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	popq	%rcx                    # encoding: [0x59]
	retq                            # encoding: [0xc3]
.Lfunc_end0:
	.size	vecsum, .Lfunc_end0-vecsum
	.cfi_endproc

	.type	B,@object               # @B
	.comm	B,8192,64
	.type	C,@object               # @C
	.comm	C,8192,64
	.type	A,@object               # @A
	.comm	A,8192,64
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Result = %d\n"
	.size	.L.str, 13

	.globl	PIM_ADDR
	.data
	.align 16
	.type	PIM_ADDR,@object      # @PIM_ADDR
	.size	PIM_ADDR, 512


	.globl	PIM_LS
	.data
	.align 16
	.type	PIM_LS,@object      # @PIM_LS
	.size	PIM_LS, 512


	.section	".note.GNU-stack","",@progbits
