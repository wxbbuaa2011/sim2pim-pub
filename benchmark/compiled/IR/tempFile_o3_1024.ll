; ModuleID = '../../benchmark/compiled/IR/tempFile.ll'
source_filename = "../../benchmark/tempFile.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@B = common global [2097152 x i32] zeroinitializer, align 64
@C = common global [2097152 x i32] zeroinitializer, align 64
@A = common local_unnamed_addr global [2097152 x i32] zeroinitializer, align 64
@.str = private unnamed_addr constant [13 x i8] c"Result = %d\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @vecsum() local_unnamed_addr #0 {
  br label %1

; <label>:1:                                      ; preds = %1, %0
  %indvars.iv32 = phi i64 [ 0, %0 ], [ %indvars.iv.next33, %1 ]
  %2 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %indvars.iv32
  %3 = trunc i64 %indvars.iv32 to i32
  store i32 %3, i32* %2, align 4, !tbaa !1
  %4 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %indvars.iv32
  %indvars.iv32.tr = trunc i64 %indvars.iv32 to i32
  %5 = shl i32 %indvars.iv32.tr, 1
  store i32 %5, i32* %4, align 4, !tbaa !1
  %indvars.iv.next33 = add nuw nsw i64 %indvars.iv32, 1
  %exitcond34 = icmp eq i64 %indvars.iv.next33, 2097152
  br i1 %exitcond34, label %.preheader.preheader, label %1, !llvm.loop !5

.preheader.preheader:                             ; preds = %1
  br label %.preheader

min.iters.checked:                                ; preds = %.preheader
  tail call void @llvm.x86.sse2.mfence()
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %min.iters.checked
  %index = phi i64 [ 0, %min.iters.checked ], [ %index.next.7, %vector.body ]
  %6 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %index
  %7 = bitcast i32* %6 to <1024 x i32>*
  %wide.load = load <1024 x i32>, <1024 x i32>* %7, align 64, !tbaa !1
  %8 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %index
  %9 = bitcast i32* %8 to <1024 x i32>*
  %wide.load35 = load <1024 x i32>, <1024 x i32>* %9, align 64, !tbaa !1
  %10 = add nsw <1024 x i32> %wide.load35, %wide.load
  %11 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @A, i64 0, i64 %index
  %12 = bitcast i32* %11 to <1024 x i32>*
  store <1024 x i32> %10, <1024 x i32>* %12, align 64, !tbaa !1
  %index.next = or i64 %index, 1024
  %13 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %index.next
  %14 = bitcast i32* %13 to <1024 x i32>*
  %wide.load.1 = load <1024 x i32>, <1024 x i32>* %14, align 64, !tbaa !1
  %15 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %index.next
  %16 = bitcast i32* %15 to <1024 x i32>*
  %wide.load35.1 = load <1024 x i32>, <1024 x i32>* %16, align 64, !tbaa !1
  %17 = add nsw <1024 x i32> %wide.load35.1, %wide.load.1
  %18 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @A, i64 0, i64 %index.next
  %19 = bitcast i32* %18 to <1024 x i32>*
  store <1024 x i32> %17, <1024 x i32>* %19, align 64, !tbaa !1
  %index.next.1 = or i64 %index, 2048
  %20 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %index.next.1
  %21 = bitcast i32* %20 to <1024 x i32>*
  %wide.load.2 = load <1024 x i32>, <1024 x i32>* %21, align 64, !tbaa !1
  %22 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %index.next.1
  %23 = bitcast i32* %22 to <1024 x i32>*
  %wide.load35.2 = load <1024 x i32>, <1024 x i32>* %23, align 64, !tbaa !1
  %24 = add nsw <1024 x i32> %wide.load35.2, %wide.load.2
  %25 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @A, i64 0, i64 %index.next.1
  %26 = bitcast i32* %25 to <1024 x i32>*
  store <1024 x i32> %24, <1024 x i32>* %26, align 64, !tbaa !1
  %index.next.2 = or i64 %index, 3072
  %27 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %index.next.2
  %28 = bitcast i32* %27 to <1024 x i32>*
  %wide.load.3 = load <1024 x i32>, <1024 x i32>* %28, align 64, !tbaa !1
  %29 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %index.next.2
  %30 = bitcast i32* %29 to <1024 x i32>*
  %wide.load35.3 = load <1024 x i32>, <1024 x i32>* %30, align 64, !tbaa !1
  %31 = add nsw <1024 x i32> %wide.load35.3, %wide.load.3
  %32 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @A, i64 0, i64 %index.next.2
  %33 = bitcast i32* %32 to <1024 x i32>*
  store <1024 x i32> %31, <1024 x i32>* %33, align 64, !tbaa !1
  %index.next.3 = or i64 %index, 4096
  %34 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %index.next.3
  %35 = bitcast i32* %34 to <1024 x i32>*
  %wide.load.4 = load <1024 x i32>, <1024 x i32>* %35, align 64, !tbaa !1
  %36 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %index.next.3
  %37 = bitcast i32* %36 to <1024 x i32>*
  %wide.load35.4 = load <1024 x i32>, <1024 x i32>* %37, align 64, !tbaa !1
  %38 = add nsw <1024 x i32> %wide.load35.4, %wide.load.4
  %39 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @A, i64 0, i64 %index.next.3
  %40 = bitcast i32* %39 to <1024 x i32>*
  store <1024 x i32> %38, <1024 x i32>* %40, align 64, !tbaa !1
  %index.next.4 = or i64 %index, 5120
  %41 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %index.next.4
  %42 = bitcast i32* %41 to <1024 x i32>*
  %wide.load.5 = load <1024 x i32>, <1024 x i32>* %42, align 64, !tbaa !1
  %43 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %index.next.4
  %44 = bitcast i32* %43 to <1024 x i32>*
  %wide.load35.5 = load <1024 x i32>, <1024 x i32>* %44, align 64, !tbaa !1
  %45 = add nsw <1024 x i32> %wide.load35.5, %wide.load.5
  %46 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @A, i64 0, i64 %index.next.4
  %47 = bitcast i32* %46 to <1024 x i32>*
  store <1024 x i32> %45, <1024 x i32>* %47, align 64, !tbaa !1
  %index.next.5 = or i64 %index, 6144
  %48 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %index.next.5
  %49 = bitcast i32* %48 to <1024 x i32>*
  %wide.load.6 = load <1024 x i32>, <1024 x i32>* %49, align 64, !tbaa !1
  %50 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %index.next.5
  %51 = bitcast i32* %50 to <1024 x i32>*
  %wide.load35.6 = load <1024 x i32>, <1024 x i32>* %51, align 64, !tbaa !1
  %52 = add nsw <1024 x i32> %wide.load35.6, %wide.load.6
  %53 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @A, i64 0, i64 %index.next.5
  %54 = bitcast i32* %53 to <1024 x i32>*
  store <1024 x i32> %52, <1024 x i32>* %54, align 64, !tbaa !1
  %index.next.6 = or i64 %index, 7168
  %55 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %index.next.6
  %56 = bitcast i32* %55 to <1024 x i32>*
  %wide.load.7 = load <1024 x i32>, <1024 x i32>* %56, align 64, !tbaa !1
  %57 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %index.next.6
  %58 = bitcast i32* %57 to <1024 x i32>*
  %wide.load35.7 = load <1024 x i32>, <1024 x i32>* %58, align 64, !tbaa !1
  %59 = add nsw <1024 x i32> %wide.load35.7, %wide.load.7
  %60 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @A, i64 0, i64 %index.next.6
  %61 = bitcast i32* %60 to <1024 x i32>*
  store <1024 x i32> %59, <1024 x i32>* %61, align 64, !tbaa !1
  %index.next.7 = add nsw i64 %index, 8192
  %62 = icmp eq i64 %index.next.7, 2097152
  br i1 %62, label %middle.block, label %vector.body, !llvm.loop !9

.preheader:                                       ; preds = %.preheader.preheader, %.preheader
  %indvars.iv29 = phi i64 [ %indvars.iv.next30, %.preheader ], [ 0, %.preheader.preheader ]
  %63 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %indvars.iv29
  %64 = bitcast i32* %63 to i8*
  tail call void @llvm.x86.sse2.clflush(i8* %64)
  %65 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %indvars.iv29
  %66 = bitcast i32* %65 to i8*
  tail call void @llvm.x86.sse2.clflush(i8* %66)
  %indvars.iv.next30 = add nuw nsw i64 %indvars.iv29, 1
  %exitcond31 = icmp eq i64 %indvars.iv.next30, 2097152
  br i1 %exitcond31, label %min.iters.checked, label %.preheader, !llvm.loop !10

middle.block:                                     ; preds = %vector.body
  %67 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 0), align 64, !tbaa !1
  %68 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 1048576), align 64, !tbaa !1
  %69 = add nsw i32 %68, %67
  %70 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 2097151), align 4, !tbaa !1
  %71 = add nsw i32 %69, %70
  %72 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str, i64 0, i64 0), i32 %71)
  %73 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 0), align 64, !tbaa !1
  %74 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 1048576), align 64, !tbaa !1
  %75 = add nsw i32 %74, %73
  %76 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 2097151), align 4, !tbaa !1
  %77 = add nsw i32 %75, %76
  ret i32 %77
}

; Function Attrs: nounwind
declare void @llvm.x86.sse2.clflush(i8*) #1

; Function Attrs: nounwind
declare void @llvm.x86.sse2.mfence() #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #2

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="knl" "target-features"="+adx,+aes,+avx,+avx2,+avx512cd,+avx512er,+avx512f,+avx512pf,+bmi,+bmi2,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+lzcnt,+mmx,+movbe,+pclmul,+popcnt,+prefetchwt1,+rdrnd,+rdseed,+rtm,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsaveopt" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="knl" "target-features"="+adx,+aes,+avx,+avx2,+avx512cd,+avx512er,+avx512f,+avx512pf,+bmi,+bmi2,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+lzcnt,+mmx,+movbe,+pclmul,+popcnt,+prefetchwt1,+rdrnd,+rdseed,+rtm,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsaveopt" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.1-10 (tags/RELEASE_401/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = distinct !{!5, !6, !7, !8}
!6 = !{!"llvm.loop.vectorize.width", i32 1}
!7 = !{!"llvm.loop.interleave.count", i32 1}
!8 = !{!"llvm.loop.unroll.disable"}
!9 = distinct !{!9, !6, !7}
!10 = distinct !{!10, !6, !7, !8}
