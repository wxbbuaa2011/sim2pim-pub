; ModuleID = '../../benchmark/tempFile.c'
source_filename = "../../benchmark/tempFile.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@.str = private unnamed_addr constant [13 x i8] c"Result = %d\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @vecsum() #0 {
  %1 = alloca i32*, align 8
  %2 = alloca i32*, align 8
  %3 = alloca i32*, align 8
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = bitcast i32** %1 to i8*
  call void @llvm.lifetime.start(i64 8, i8* %6) #4
  %7 = bitcast i32** %2 to i8*
  call void @llvm.lifetime.start(i64 8, i8* %7) #4
  %8 = bitcast i32** %3 to i8*
  call void @llvm.lifetime.start(i64 8, i8* %8) #4
  %9 = call noalias i8* @malloc(i64 134217728) #4
  %10 = bitcast i8* %9 to i32*
  store i32* %10, i32** %1, align 8, !tbaa !1
  %11 = call noalias i8* @malloc(i64 134217728) #4
  %12 = bitcast i8* %11 to i32*
  store i32* %12, i32** %2, align 8, !tbaa !1
  %13 = call noalias i8* @malloc(i64 134217728) #4
  %14 = bitcast i8* %13 to i32*
  store i32* %14, i32** %3, align 8, !tbaa !1
  %15 = bitcast i32* %4 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %15) #4
  store i32 0, i32* %4, align 4, !tbaa !5
  br label %16

; <label>:16:                                     ; preds = %37, %0
  %17 = load i32, i32* %4, align 4, !tbaa !5
  %18 = icmp slt i32 %17, 33554432
  br i1 %18, label %21, label %19

; <label>:19:                                     ; preds = %16
  %20 = bitcast i32* %4 to i8*
  call void @llvm.lifetime.end(i64 4, i8* %20) #4
  br label %40

; <label>:21:                                     ; preds = %16
  %22 = load i32*, i32** %2, align 8, !tbaa !1
  %23 = load i32, i32* %4, align 4, !tbaa !5
  %24 = sext i32 %23 to i64
  %25 = getelementptr inbounds i32, i32* %22, i64 %24
  %26 = load i32, i32* %25, align 4, !tbaa !5
  %27 = load i32*, i32** %3, align 8, !tbaa !1
  %28 = load i32, i32* %4, align 4, !tbaa !5
  %29 = sext i32 %28 to i64
  %30 = getelementptr inbounds i32, i32* %27, i64 %29
  %31 = load i32, i32* %30, align 4, !tbaa !5
  %32 = add i32 %26, %31
  %33 = load i32*, i32** %1, align 8, !tbaa !1
  %34 = load i32, i32* %4, align 4, !tbaa !5
  %35 = sext i32 %34 to i64
  %36 = getelementptr inbounds i32, i32* %33, i64 %35
  store i32 %32, i32* %36, align 4, !tbaa !5
  br label %37

; <label>:37:                                     ; preds = %21
  %38 = load i32, i32* %4, align 4, !tbaa !5
  %39 = add nsw i32 %38, 1
  store i32 %39, i32* %4, align 4, !tbaa !5
  br label %16

; <label>:40:                                     ; preds = %19
  %41 = bitcast i32* %5 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %41) #4
  %42 = load i32*, i32** %1, align 8, !tbaa !1
  %43 = getelementptr inbounds i32, i32* %42, i64 0
  %44 = load i32, i32* %43, align 4, !tbaa !5
  %45 = load i32*, i32** %1, align 8, !tbaa !1
  %46 = getelementptr inbounds i32, i32* %45, i64 16777216
  %47 = load i32, i32* %46, align 4, !tbaa !5
  %48 = add i32 %44, %47
  %49 = load i32*, i32** %1, align 8, !tbaa !1
  %50 = getelementptr inbounds i32, i32* %49, i64 33554431
  %51 = load i32, i32* %50, align 4, !tbaa !5
  %52 = add i32 %48, %51
  store i32 %52, i32* %5, align 4, !tbaa !5
  %53 = load i32, i32* %5, align 4, !tbaa !5
  %54 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str, i32 0, i32 0), i32 %53)
  %55 = load i32*, i32** %1, align 8, !tbaa !1
  %56 = bitcast i32* %55 to i8*
  call void @free(i8* %56) #4
  %57 = load i32*, i32** %2, align 8, !tbaa !1
  %58 = bitcast i32* %57 to i8*
  call void @free(i8* %58) #4
  %59 = load i32*, i32** %3, align 8, !tbaa !1
  %60 = bitcast i32* %59 to i8*
  call void @free(i8* %60) #4
  %61 = load i32, i32* %5, align 4, !tbaa !5
  %62 = bitcast i32* %5 to i8*
  call void @llvm.lifetime.end(i64 4, i8* %62) #4
  %63 = bitcast i32** %3 to i8*
  call void @llvm.lifetime.end(i64 8, i8* %63) #4
  %64 = bitcast i32** %2 to i8*
  call void @llvm.lifetime.end(i64 8, i8* %64) #4
  %65 = bitcast i32** %1 to i8*
  call void @llvm.lifetime.end(i64 8, i8* %65) #4
  ret i32 %61
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #2

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

declare i32 @printf(i8*, ...) #3

; Function Attrs: nounwind
declare void @free(i8*) #2

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="knl" "target-features"="+adx,+aes,+avx,+avx2,+avx512cd,+avx512er,+avx512f,+avx512pf,+bmi,+bmi2,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+lzcnt,+mmx,+movbe,+pclmul,+popcnt,+prefetchwt1,+rdrnd,+rdseed,+rtm,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsaveopt" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="knl" "target-features"="+adx,+aes,+avx,+avx2,+avx512cd,+avx512er,+avx512f,+avx512pf,+bmi,+bmi2,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+lzcnt,+mmx,+movbe,+pclmul,+popcnt,+prefetchwt1,+rdrnd,+rdseed,+rtm,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsaveopt" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="knl" "target-features"="+adx,+aes,+avx,+avx2,+avx512cd,+avx512er,+avx512f,+avx512pf,+bmi,+bmi2,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+lzcnt,+mmx,+movbe,+pclmul,+popcnt,+prefetchwt1,+rdrnd,+rdseed,+rtm,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsaveopt" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.1-10 (tags/RELEASE_401/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"any pointer", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"int", !3, i64 0}
