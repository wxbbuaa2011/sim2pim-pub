#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
// #include <x86intrin.h>
#include <emmintrin.h>
#include <immintrin.h>
#include <stdint.h>

#define N 2048*1024

__attribute__ ((aligned(64))) int A[N];
__attribute__ ((aligned(64))) int B[N];
__attribute__ ((aligned(64))) int C[N];

int main(void)
{
    int a;
    int b;
    #pragma clang loop unroll(disable) vectorize(disable) interleave(disable)
    for(int i=0; i<N; i++) {
        B[i] = i;
        C[i] = i*2;
    }
    
    #pragma clang loop unroll(disable) vectorize(disable) interleave(disable)
    for(int i=0; i<N; i++) {
        _mm_clflush(&B[i]);
        _mm_clflush(&C[i]);
    }
    _mm_mfence();


    //THIS IS WHAT WE WANT TO MEASURE
    for(int i=0; i<N; i++) {
        A[i] = B[i] + C[i];
    }

    printf("Result = %d\n", (A[0] + A[N/2] + A[N-1]));

    return (A[0] + A[N/2] + A[N-1]);
}
