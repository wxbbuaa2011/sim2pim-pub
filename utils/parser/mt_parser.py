#!/usr/bin/python

import sys
import re

##MODIFY .FILE NAME

input_asm = sys.argv[1]
output_asm = sys.argv[2]

input_file  = open(input_asm, "r")
# bench_type = "vecsum"
end=False
RVU_NUM = 32
output_file = open(output_asm, "w")

join = []
create = []
join.append(0)
create.append(0)
new_block = 0
is_threaded = False
for line in input_file:
    if re.match("(.*)callq	pthread_create(.*)",line):
        if create[-1] == 0 or new_block == 0:
            create.append(0)
            new_block = 1
        create[-1] = create[-1] +1
        is_threaded = True
        # print line
    elif re.match("(.*)callq	pthread_join(.*)",line):
        if join[-1] == 0 or new_block == 1:
            join.append(0)
            new_block = 0
        join[-1] = join[-1] +1
        is_threaded = True

join.pop(0)
create.pop(0)

if is_threaded:
    RVU_per_Core = RVU_NUM/join[0]
else:
    RVU_per_Core = 32


if len(create) != len(join):
    print "Different Sized blocks!!!!"
    exit()

input_file.close()
output_file = open(output_asm, "w")
input_file  = open(input_asm, "r")
count = 0


line = "#rvu per cores for MT = "+str(RVU_per_Core)+'\n'
output_file.write(line)

if is_threaded is False:
    for line in input_file:
        output_file.write("%s" % line)
    output_file.close()
    print "Not threaded!"
    exit()

in_thread = False

for line in input_file:
    new_line = line

    if re.match("(.*)callq	pthread_create(.*)",line):
        new_line = new_line.replace("pthread_create", "WrapperCreate")

    if re.match("(.*)callq	pthread_join(.*)",line):
        new_line = new_line.replace("pthread_join", "WrapperJoin")

    if re.match("^Thread_Call.:",line):
        in_thread = True

    if in_thread:
        if re.match("(.*)PIM(.*)",line):
            new_line = ''
            for word in line.split():
                # print word
                if word.find('%RVU_') == 0:

                    pos1 = word[5:].find('R')
                    temp = word[5:pos1+5]
                    temp = re.sub(
                            '\d+',
                            lambda m: '{}'.format(int(m.group())+(create[0]-1)*(RVU_NUM/join[0])),
                            temp[:])

                    s = re.search(r"\d+(\d+)?", temp)
                    first_RVU = s.group(0)

                    new_word = word[:5] + temp + word[pos1+5:]
                else:
                    new_word = word


                new_line = new_line +" "+ new_word
                # print new_line, '\n'
            new_line = new_line +" "+ "#first_RVU: "+first_RVU +"\n"

    if re.match("(.*)callq	pthread_exit(.*)",line):
        in_thread = False
        create[0] = create[0]-1

    output_file.write("%s" % new_line)

output_file.close()
input_file.close()
