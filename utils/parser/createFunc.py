#!/usr/bin/python

import sys
import re

input_asm = sys.argv[1]
bench_type = sys.argv[2]

input_file  = open(input_asm, "r")
# bench_type = "vecsum"
end=False

output_file = open("../../benchmark/tempFile.c", "w")

for line in input_file:
    new_line = line
    if end is False:
        if re.match("(.*)main\((.*)",line):
            new_line = new_line.replace("main(", bench_type+"(")
            end=True

    output_file.write("%s" % new_line)

output_file.close()
input_file.close()
