#!/bin/bash

###### sh exec.sh <SOURCE_CODE_NAME> <SSE> <AVX> <RVU_SIZE_IN_INTEGERS>

########### EXAMPLE OF USE
###### sh exec.sh vecsum SSE AVX 32 --> vecsum.c compiled for SSE, AVX, and RVU_128B
###### sh exec.sh vecsum 0 AVX 64 --> vecsum.c compiled for AVX and RVU_256B
###### sh exec.sh vecsum SSE AVX 0 --> vecsum.c compiled for SSE and AVX


C_CODE_NAME=$1
SSE_COMPILING=$2
AVX_COMPILING=$3
RVU_SIZE=$4
temp=0
cte=4

primo_build="/home/beforlin/pim/PRIMO/pim_v2/build"

IR="../../benchmark/compiled/IR"

ASM="../../benchmark/compiled/ASM"

BENCHMARK_FOLDER="../../benchmark"

clear

printf $C_CODE_NAME

########################################################################
printf "...Compiling with LLVM for X86_SandyBridge, X86_KNL and X86_KNL+RVU...\n"

# printf "Memory Information\n"
# free -m
# printf "Disk Information\n"
# df -h
# printf "\n"
# printf "\n"

########################################################################
printf "===================================================\n"
printf "COMPILING %s " "$C_CODE_NAME"
printf "for the following archs\n"
if [ $SSE_COMPILING = "SSE" ];
then
	printf "SSE\n"
fi
if [ $AVX_COMPILING = "AVX" ];
then
	printf "AVX\n"
fi
if [ $RVU_SIZE -ne 0 ];
then
	printf "RVU %d Bytes\n" "$(($RVU_SIZE*$cte))"
fi
printf "\n"
printf "\n"


########################################################################

printf "Clang - Generating LLVM intermediate language file\n"
clang-4.0 -S -emit-llvm -march=knl -O3 -mllvm -disable-llvm-optzns $BENCHMARK_FOLDER/$C_CODE_NAME.c -o $IR/$C_CODE_NAME.ll
# clang-4.0 -S -emit-llvm -march=knl -O3 -mllvm -disable-llvm-optzns gem5_stats.c -o IR/gem5_stats.ll
printf "Clang - ll generated\n\n"

##################### OPTIMIZATION ######################
printf "LLVM Opt - Optimizing intermediate language file\n"
# /home/pcssjunior/llvm/llvm_build/bin/opt -S -O3 IR/gem5_stats.ll -o IR/gem5_stats_o3.ll
if [ $RVU_SIZE -ne 0 ]; then
    $primo_build/bin/opt -S -O3 -force-vector-width=$RVU_SIZE -pass-remarks=loop-vectorize -enable-load-pre=0 $IR/$C_CODE_NAME.ll -o $IR/$C_CODE_NAME\_o3\_$RVU_SIZE.ll
fi
if [ $SSE_COMPILING = "SSE" ] | [ $AVX_COMPILING = "AVX" ]; then
    $primo_build/bin/opt -S -O3 -pass-remarks=loop-vectorize -enable-load-pre=0 $IR/$C_CODE_NAME.ll -o $IR/$C_CODE_NAME\_o3\_X86.ll
fi
printf "LLVM Opt - ll o3 level optimization generated\n\n"

temp=$(($RVU_SIZE*$cte))

##################### ASM ######################
printf "LLC- Generating ASM codes\n"
# /$primo_build/bin/llc -filetype=asm -show-mc-encoding -x86-asm-syntax=intel -mcpu=sandybridge -mattr=-avx,-avx2,-avx512f IR/gem5_stats_o3.ll -o ./ASM/gem5_stats.s
if [ $SSE_COMPILING = "SSE" ]; then
	# $primo_build/bin/llc -filetype=asm -show-mc-encoding -x86-asm-syntax=intel -mcpu=sandybridge -mattr=-avx,-avx2,-avx512f IR/$C_CODE_NAME\_o3\_X86.ll -o ./ASM/$C_CODE_NAME\_SSE.s
	$primo_build/bin/llc -filetype=asm -show-mc-encoding -mcpu=sandybridge -mattr=-avx,-avx2,-avx512f $IR/$C_CODE_NAME\_o3\_X86.ll -o $ASM/$C_CODE_NAME\_SSE.s
fi
if [ $AVX_COMPILING = "AVX" ]; then
	# $primo_build/bin/llc -filetype=asm -show-mc-encoding -x86-asm-syntax=intel -mcpu=knl IR/$C_CODE_NAME\_o3\_X86.ll -o ./ASM/$C_CODE_NAME\_AVX.s
	$primo_build/bin/llc -filetype=asm -show-mc-encoding -mcpu=knl $IR/$C_CODE_NAME\_o3\_X86.ll -o $ASM/$C_CODE_NAME\_AVX.s
fi
if [ $RVU_SIZE -ne 0 ]; then
	# $primo_build/bin/llc -filetype=asm -show-mc-encoding -x86-asm-syntax=intel -mattr=+pim,+avx512bw IR/$C_CODE_NAME\_o3\_$RVU_SIZE.ll -o ./ASM/$C_CODE_NAME\_VPUOFF\_RVU$(($RVU_SIZE*$cte))B.s
	$primo_build/bin/llc -filetype=asm -show-mc-encoding -mattr=+pim,+avx512bw $IR/$C_CODE_NAME\_o3\_$RVU_SIZE.ll -o $ASM/$C_CODE_NAME\_$(($RVU_SIZE*$cte))B.s
fi
printf "LLC - ASM codes generated\n\n"

printf "ALL FINISHED\n\n\n"

#~ rm *.ll
#~ rm *.o
