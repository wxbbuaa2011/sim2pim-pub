#!/bin/bash

BENCHMARK=$1

######################## PLEASE DO NOT EDIT HERE ##############################
if [[ $# -lt 1 ||  $# -gt 1 ]]; then
    echo "Illegal number of parameters"
    echo "SELECT THE BENCHMARK ASM FILE e.g., bash this_script VECSUM_XXX.s"
    exit 2
fi

PARSERDIR="utils/parser"
TESTS_SRC_FOLDER="benchmark"
PARSED_TESTS_FOLDER="benchmark/src"

if python ./$PARSERDIR/parser.py ./$BENCHMARK ./$PARSED_TESTS_FOLDER/x86_$BENCHMARK ; then
  echo "Parsed to ./$PARSED_TESTS_FOLDER/x86_$BENCHMARK";
else
  echo -e "\n\e[31mParser failed\e[0m\n";
  exit $?
fi
