# Kernel RPMC #

Enables counter access in CR4:

* $ make
* $ sudo insmod ./PMC_kernel.ko

If you want to remove:

* $ sudo rmmod PMC_kernel
