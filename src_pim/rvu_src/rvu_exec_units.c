

#include "rvu_exec_units.h"

void rvu_decode_stage(void){
    #if (PRINTS_EX == true)
        printf("RVU_EXEC_UNITS: Decode Stage \n");
    #endif
}


void rvu_exec_stage(void){
    #if (PRINTS_EX == true)
        printf("RVU_EXEC_UNITS: Execution Stage \n");
    #endif
}



void rvu_exec_units(void) {
    
    int flag_temp = 0;
    
    uint32_t i,j,k;
    uint64_t EX_CURRENT_CY = 0;
    
    uint32_t * addr_dword_temp;
    uint32_t operand_dst_dword_temp = 0;
    uint32_t operand_src1_dword_temp = 0;
    uint32_t operand_src2_dword_temp = 0;    
    uint64_t * addr_qword_temp;
    uint64_t operand_dst_qword_temp = 0;
    uint64_t operand_src1_qword_temp = 0;
    uint64_t operand_src2_qword_temp = 0;
    
    
        
    #if (PINTS_EX_ENTER == true)
        printf("\n\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
              "Entering RVU_EXEC_UNITS:\n"
              "Printing in-flight Instructions\n");
    #endif
    

    //---------------- ROUND ROBIN through each PIM unit ------------------//
    for (i=0; i< NUMBER_OF_RVU_UNITS; i++) {
        #if (PINTS_EX == true)
            printf("CHEKING RVU UNIT %d = %d \n",i, IM_OUT_BUFFER[i].valid);
        #endif
        
        if(IM_OUT_BUFFER[i].valid == true) { //checks which PIM unit will run in this cycle
            ////////////////////////////////////////
            //DECODE STAGE
            #if (METRICS_ON == true)
                // EX_PREVIOUS_CY = EXEC_UNIT_PRE_DECODER_CY
                EX_CURRENT_CY = EXEC_UNIT_PRE_DECODER_CY + EX_CURRENT_CY;
            #endif
            
            #if (PRINTS_EX == true)
                //printf("DECODING ON UNIT_%d - valid = %d\n",i, IM_OUT_BUFFER[i].valid);
                printf("DECODING ON UNIT_%d\n",i);                
            #endif
            if((IM_OUT_BUFFER[i].OPERATION == LOAD) || (IM_OUT_BUFFER[i].OPERATION == STORE)) { //TODO: broadcast 
                if(IM_OUT_BUFFER[i].OPER_TYPE == TDWORD){
                    addr_dword_temp = (uint32_t *) IM_OUT_BUFFER[i].LS_ADDR;
                    #if (PRINTS_EX == true)
                        printf("DECODING LS FOR TYPE DWORD on Unit_%d - accessing address %p\n", i, addr_dword_temp);
                    #endif                    
                }
                else if(IM_OUT_BUFFER[i].OPER_TYPE == TQWORD){
                    addr_qword_temp = (uint64_t *) IM_OUT_BUFFER[i].LS_ADDR;
                    #if (PRINTS_EX == true)
                        printf("DECODING LS FOR TYPE QDWORD accessing address %p\n", addr_qword_temp);
                    #endif                                  
                }
                else {
                    exit(1); //print err() TYPE NOT IMPLEMENTED
                }
            }            

            ////////////////////////////////////////
            //EXCUTION STAGE
            #if (METRICS_ON == true)
                // EX_PREVIOUS_CY = EXEC_UNIT_PRE_DECODER_CY
                EX_CURRENT_CY = EXEC_UNIT_PRE_DECODER_CY + EX_CURRENT_CY;
            #endif
            
            #if (METRIC_PRINT == true)
                printf("EX_CURRENT_CY = %lu\n", EX_CURRENT_CY);
            #endif
                
            switch (IM_OUT_BUFFER[i].OPERATION) {
                //LOAD INSTRUCTION
                case LOAD:
                    #if (METRICS_PRINTS == true)
                        //
                    #endif
                    #if (PRINTS_EX == true)
                        printf("LOAD on Unit_%d -> EX_CURRENT_CY = %lu\n", i, EX_CURRENT_CY);
                    #endif
                    
                    if(IM_OUT_BUFFER[i].OPER_TYPE == TDWORD) {
                        for(j=0; j < (2<<(IM_OUT_BUFFER[i].OPER_SIZE+1)); j+=(1<<IM_OUT_BUFFER[i].OPER_TYPE)) { //iterate over OPER_SIZE - max 256Bytes
                            operand_dst_dword_temp = *addr_dword_temp; //read from memory
                            #if (PRINTS_EX == true)
                                if(flag_temp == 0) { //just print the first operation of each PIM unit
                                    printf("PRINTING LOAD VALUE on Unit_%d = %d\n", i, operand_dst_dword_temp);
                                    // flag_temp = 1;
                                }                                    
                            #endif 
                            
                            for (k=0; k < (1<<IM_OUT_BUFFER[i].OPER_TYPE); k++){
                                RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_DST].RegFile[IM_OUT_BUFFER[i].REG_DST][j+k] = (uint8_t) operand_dst_dword_temp;
                                operand_dst_dword_temp = (operand_dst_dword_temp >> 8);
                                #if (PRINTS_EX == true)
                                    if (flag_temp == 0) {
                                        printf("LOAD => RVU_REG_FILE[%d].RegFile[%d][%d] = %d --> on Unit_%d = %d\n", IM_OUT_BUFFER[i].RVU_DST,IM_OUT_BUFFER[i].REG_DST, j+k, RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_DST].RegFile[IM_OUT_BUFFER[i].REG_DST][j+k], i, operand_dst_dword_temp);
                                    }
                                #endif 
                            }
                            addr_dword_temp++;
                            flag_temp = 1;
                        }
                    } else if(IM_OUT_BUFFER[i].OPER_TYPE == TQWORD) {
                            for(j=0; j < (2<<(IM_OUT_BUFFER[i].OPER_SIZE+1)); j+=(1<<IM_OUT_BUFFER[i].OPER_TYPE)) { //iterate over OPER_SIZE - max 256Bytes
                                operand_dst_qword_temp = *addr_qword_temp; //read from memory
                            
                            for (k=0; k < (1<<IM_OUT_BUFFER[i].OPER_TYPE); k++){
                                RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_DST].RegFile[IM_OUT_BUFFER[i].REG_DST][j+k] = (uint8_t) operand_dst_qword_temp;
                                operand_dst_qword_temp = (operand_dst_qword_temp >> 8);
                            }
                            addr_qword_temp++;
                            #if (PRINTS_EX == true)
                                printf("PRINTING LOAD VALUE = %ld\n", operand_dst_qword_temp);
                            #endif                            
                        }
                    } else {
                        exit(1); //print err TYPE NOT IMPLEMENTED
                    }
                    flag_temp = 0;
                    break;
                    
                //STORE INSTRUCTION
                case STORE: 
                    #if (METRICS_PRINTS == true)
                        //
                    #endif
                    #if (PRINTS_EX == true)
                        printf("STORE on Unit_%d -> EX_CURRENT_CY = %lu\n", i, EX_CURRENT_CY);
                    #endif
                    operand_dst_dword_temp = 0;
                    if(IM_OUT_BUFFER[i].OPER_TYPE == TDWORD) {
                        for(j=0; j < (2<<(IM_OUT_BUFFER[i].OPER_SIZE+1)); j+=(1<<IM_OUT_BUFFER[i].OPER_TYPE)) { //iterate over OPER_SIZE - max 256Bytes
                            for (k= (1<<IM_OUT_BUFFER[i].OPER_TYPE); k > 0; k--){
                                operand_dst_dword_temp = (operand_dst_dword_temp << 8);
                                operand_dst_dword_temp = operand_dst_dword_temp | ((uint32_t) RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_DST].RegFile[IM_OUT_BUFFER[i].REG_DST][j+k-1]) ;
                                #if (PRINTS_EX == true)
                                    if (flag_temp == 0) {
                                        printf("STORE => RVU_REG_FILE[%d].RegFile[%d][%d] = %d --> on Unit_%d = %d\n", IM_OUT_BUFFER[i].RVU_DST,IM_OUT_BUFFER[i].REG_DST, j+k-1, RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_DST].RegFile[IM_OUT_BUFFER[i].REG_DST][j+k-1], i, operand_dst_dword_temp);
                                    }
                                #endif                                 
                            }
                            *addr_dword_temp = operand_dst_dword_temp; //write to memory
                            addr_dword_temp++;
                            #if (PRINTS_EX == true)
                                if(flag_temp == 0) {                            
                                    printf("PRINTING STORE VALUE on Unit_%d = %d\n", i, operand_dst_dword_temp);
                                    flag_temp = 1; 
                                }
                            #endif                                          
                        }
                    } else if(IM_OUT_BUFFER[i].OPER_TYPE == TQWORD) {
                        for(j=0; j < (2<<(IM_OUT_BUFFER[i].OPER_SIZE+1)); j+=(1<<IM_OUT_BUFFER[i].OPER_TYPE)) { //iterate over OPER_SIZE - max 256Bytes
                            for (k= (1<<IM_OUT_BUFFER[i].OPER_TYPE); k > 0; k--){
                                operand_dst_qword_temp = (operand_dst_qword_temp << 8);
                                operand_dst_qword_temp = operand_dst_qword_temp | ((uint64_t) RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_DST].RegFile[IM_OUT_BUFFER[i].REG_DST][j+k-1]);
                            }
                            *addr_qword_temp = operand_dst_qword_temp; //write to memory
                            addr_qword_temp++;
                            #if (PRINTS == true)
                                printf("PRINTING STORE VALUE = %ld\n", operand_dst_qword_temp);
                            #endif                                 
                        }
                    } else {
                        exit(1); //print err TYPE NOT IMPLEMENTED
                    }                    
                    flag_temp = 0;
                    break;
                
                //VADD INSTRUCTION
                case VADD: 
                    #if (METRICS_PRINTS == true)
                        //
                    #endif
                    #if (PRINTS_EX == true)
                        printf("VADD on Unit_%d -> EX_CURRENT_CY = %lu\n", i, EX_CURRENT_CY);
                    #endif
                    if(IM_OUT_BUFFER[i].OPER_TYPE == TDWORD) {
                        for(j=0; j < (2<<(IM_OUT_BUFFER[i].OPER_SIZE+1)); j+=(2<<(IM_OUT_BUFFER[i].OPER_TYPE-1))) { //iterate over OPER_SIZE - max 256Bytes
                            operand_src1_dword_temp = 0;
                            operand_src2_dword_temp = 0;
                            operand_src1_qword_temp = 0;
                            operand_src2_qword_temp = 0;
                                                        
                            for (k=(1<<IM_OUT_BUFFER[i].OPER_TYPE); k > 0; k--) { //get the source 1 registers
                                operand_src1_dword_temp = operand_src1_dword_temp << 8;
                                operand_src1_dword_temp = operand_src1_dword_temp | ((uint32_t) RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_SRC1].RegFile[IM_OUT_BUFFER[i].REG_SRC1][j+k-1]);
                                #if (PRINTS_EX == true)
                                    if (flag_temp == 0) {
                                        printf("VADD SRC1 => RVU_REG_FILE[%d].RegFile[%d][%d] = %d --> on Unit_%d = %d\n", IM_OUT_BUFFER[i].RVU_SRC1,IM_OUT_BUFFER[i].REG_SRC1, j+k-1, RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_SRC1].RegFile[IM_OUT_BUFFER[i].REG_SRC1][j+k-1], i, operand_src1_dword_temp);
                                    }
                                #endif
                            }                                
                            for (k=(1<<IM_OUT_BUFFER[i].OPER_TYPE); k > 0; k--) { //get the source 2 registers
                                operand_src2_dword_temp = operand_src2_dword_temp << 8;
                                operand_src2_dword_temp = operand_src2_dword_temp | ((uint32_t) RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_SRC2].RegFile[IM_OUT_BUFFER[i].REG_SRC2][j+k-1]);
                                #if (PRINTS_EX == true)
                                    if (flag_temp == 0) {
                                        printf("VADD SRC2 => RVU_REG_FILE[%d].RegFile[%d][%d] = %d --> on Unit_%d = %d\n", IM_OUT_BUFFER[i].RVU_SRC2,IM_OUT_BUFFER[i].REG_SRC2, j+k-1, RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_SRC2].RegFile[IM_OUT_BUFFER[i].REG_SRC2][j+k-1], i, operand_src2_dword_temp);
                                    }
                                #endif                                                 
                            }
                            
                            //then operates over the sources
                            operand_dst_dword_temp = operand_src1_dword_temp + operand_src2_dword_temp;
                            #if (PRINTS_EX == true)
                                if (flag_temp == 0) {
                                    printf("PRINTING VADD on Unit_%d => %d + %d = %d\n", i, operand_src1_dword_temp, operand_src2_dword_temp, operand_dst_dword_temp);
                                    flag_temp = 1;
                                }
                            #endif
                            
                            for (k=0; k < (2<<(IM_OUT_BUFFER[i].OPER_TYPE-1)); k++){
                                // #if (PRINTS == true)
                                    // printf("VADD DST => RVU_REG_FILE[%d].RegFile[%d][%d] = %d --> on Unit_%d = %d\n", IM_OUT_BUFFER[i].RVU_DST,IM_OUT_BUFFER[i].REG_DST, j+k, RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_DST].RegFile[IM_OUT_BUFFER[i].REG_DST][j+k], i, operand_dst_dword_temp);
                                // #endif             
                            
                                RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_DST].RegFile[IM_OUT_BUFFER[i].REG_DST][j+k] = (uint8_t) operand_dst_dword_temp;
                                operand_dst_dword_temp = (operand_dst_dword_temp >> 8);
                            }
                        }
                    } else if (IM_OUT_BUFFER[i].OPER_TYPE == TQWORD) {
                        for(j=0; j < (2<<(IM_OUT_BUFFER[i].OPER_SIZE+1)); j+=(2<<(IM_OUT_BUFFER[i].OPER_TYPE-1))) { //iterate over OPER_SIZE - max 256Bytes
                                                        
                            for (k=(2<<(IM_OUT_BUFFER[i].OPER_TYPE-1)); k > 0; k--) { //first get the source registers
                                operand_src1_qword_temp = operand_src1_qword_temp >> 8;
                                operand_src1_qword_temp = operand_src1_qword_temp | ((uint64_t) RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_DST].RegFile[IM_OUT_BUFFER[i].REG_DST][j+k-1]);
                                
                                operand_src2_qword_temp = operand_src2_qword_temp >> 8;
                                operand_src2_qword_temp = operand_src2_qword_temp | ((uint64_t) RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_DST].RegFile[IM_OUT_BUFFER[i].REG_DST][j+k-1]);
                            }
                            
                            //then operates over the sources
                            operand_dst_qword_temp = operand_src1_qword_temp + operand_src2_qword_temp;
                            
                            for (k=0; k < (2<<(IM_OUT_BUFFER[i].OPER_TYPE-1)); k++){
                                RVU_REG_FILE[IM_OUT_BUFFER[i].RVU_DST].RegFile[IM_OUT_BUFFER[i].REG_DST][j+k] = (uint8_t) operand_dst_qword_temp;
                                operand_dst_qword_temp = (operand_dst_qword_temp >> 8);
                            }
                            // #if (PRINTS == true)
                                // printf("PRINTING VADD VALUE = %ld\n", operand_dst_qword_temp);
                            // #endif
                        }
                    } else {
                        exit(1); //print err TYPE NOT IMPLEMENTED
                    }
                    flag_temp = 0;
                    break;
                 
                //OOPS    
                default:
                    printf("Why I'm here?\n");
            }
        }
    }
    
    
    
    
    

}