#ifndef LOG_GENERATOR_H
#define LOG_GENERATOR_H

#include <stdio.h>
#include <inttypes.h>

// #include "rvu.h"
// #include "instruction_manager.h"
// #include "rvu_exec_units.h"

// struct IM_LOG{
    // char Module = [];
    // uint64_t cycle;
    // uint64_t previous_cy;
    // uint64_t current_cy;
    // uint64_t cycles_diff;    
    // uint64_t INST0;
    // uint64_t INST1;
    // uint64_t ADDR;
// }


/** FROM IM MODULE **/
extern uint64_t IM_CURRENT_CY;
extern uint64_t IM_PREVIOUS_CY;


// void log_capture(char *module, uint64_t cycle);
void print_log(const char* LABEL, const uint8_t IM_PRINTS_LvL, const uint8_t EXEC_PRINTS_LvL);

// void print_log(struct )


#endif