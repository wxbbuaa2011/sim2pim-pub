/*
    THIS FILE CONTAINS:
    THE FUNCTION PROTOTYPES FOR RVU SIMULATION
    THE SPEFICICATIONS FOR RVU PIM
    THE VARIABLES REPRESENTING RVU HARDWARE
*/

#ifndef RVU_H
#define RVU_H

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>


#define true  1
#define false 0


/**** CONFIGURE THE PRINTS HERE ****/
#define PRINTS_IM_ENTER false
#define PRINTS_IM false
#define PINTS_EX_ENTER false
#define PRINTS_EX false
#define METRICS_ON true
#define METRIC_PRINTS false
#define LOG_ON false
#define LOG_LEVEL 1 //it can be 1, 2, or 3.


#include "instruction_manager.h"
#include "rvu_exec_units.h"
//#include "log_generator.h"


/*------------------------------ FUNCTION PROTOTYPES---------------------------*/
void main_rvu(uint64_t ins0, uint64_t ins1, uint64_t ls, uint64_t cycle);
/*-----------------------------------END---------------------------------------*/






/*----------------------------------- RVU SPECS -------------------------------*/

/***===================== RVU INSTRUCTION FORMAT =====================
---------------------------- LOAD/STORE ----------------------------
OPCODE - OPSIZE - DATA_TYPE - RVU_DST - RVU_REG_DST - 000's | 000's
   8       5          3          5          14         29      64
------------------------------- VADD -------------------------------
OPCODE - OPSIZE - DATA_TYPE - RVU_DST - RVU_REG_DST - RVU_SRC1 - RVU_REG_SRC1 - RVU_SRC2 - RVU_REG_SRC2 | SRC2/PART2 - 000's
   8       5          3          5          14           5            14           5            5           9       55
===================================================================***/


//NUMBER OF RVU UNITS
#define NUMBER_OF_RVU_UNITS   32


//RVU INSTRUCTION FIELD IN BITS
#define OPCODE              8
#define OP_SIZE             5
#define DATA_TYPE           3
#define RVU_ADDRESS         5
#define RVU_REG_ADDRESS     14

//RVU OP_SIZE (BYTES) LIST
#define B4          0
#define B8          1
#define B16         2
#define B32         3
#define B64         4
#define B128        5
#define B256        6
#define B512        7
#define B1024       8
#define B2048       9
#define B4096       10
#define B8192       11

//RVU DATA_TYPE LIST
#define TBYTE       0   //8bits = 1Byte
#define TWORD       1   //16bits = 2Bytes --- INT
#define THWORD      1   //16bits = 2Bytes --- FP
#define TDWORD      2   //32bits = 4Bytes
#define TQWORD      3   //64bits = 8Bytes
#define TOWORD      4   //128bits = 16Bytes



//RVU OPCODE LIST - INTEGER INSTRUCTIONS
//THIS LIST MUST MATCH TO COMPILER OR AT LEAST TO THE PARSER
enum OP_CODE{
            LOAD,           //0
            STORE,          //1
            VADD,           //2
            VSUB,
            VAND,
            VOR,
            VXOR,           //6
            VNOT,
            VMUL,           //8
            VDIV,
            VMOVV,          //10
            VMOVXMMtoPIM,   //11
            VPERM,          //12
            BROADCASTS,     //13
            VSHIFTL,
            VSHIFTR,
            VSLLI,
            VSRLI,
            VMIN,
            VMAX,
};


//STRUCT TO FACILITATE THE COMMUNICATION BETWEEN INSTRUCTION_MANAGER AND PIM UNITS
typedef struct {
    uint8_t  valid;
    uint8_t  isLS;
    uint8_t  isINTERVAULT;
    uint8_t  isACQUIRE;
    uint8_t  isRELEASE;
    uint8_t  OPERATION;
    uint8_t  OPER_SIZE;
    uint8_t  OPER_TYPE;
    uint32_t RVU_DST;
    uint32_t REG_DST;
    uint32_t RVU_SRC1;
    uint32_t REG_SRC1;
    uint32_t RVU_SRC2;
    uint32_t REG_SRC2;
    uint64_t LS_ADDR;
}INSTPACK;

INSTPACK IM_OUT_BUFFER[NUMBER_OF_RVU_UNITS]; //for now 1 position per RVU 

/*-----------------------------------END---------------------------------------*/

//Register File
typedef struct {
    uint8_t RegFile[8][256]; //8 registers x 256Bytes each per RVU //byte-width register because RVU can offer byte-width operations //yes, it may be not that efficient on C, and yes, we could do using 32/64bit instead of 8bit
}REGFILE;

/* 32x register_banks - 8x256B each */
REGFILE RVU_REG_FILE[NUMBER_OF_RVU_UNITS]; 

/*-----------------------------------END---------------------------------------*/



/*------------------------------- METRICS VARIABLES ---------------------------*/
#define IM_PIM_CHECKER_CY           2
#define IM_PRE_DECODER_CY           2
#define IM_INSTRUCTION_COMPOSER_CY  2
#define IM_INSTRUCTION_SPLITTER_CY  2     
#define IM_INSTRUCTION_EMITTER_CY   2     

uint64_t IM_PREVIOUS_CY;
uint64_t IM_CURRENT_CY;
uint64_t IM_TOTAL_CY;


#define EXEC_UNIT_PRE_DECODER_CY    2
#define EXEC_UNIT_DECODER_CY        2
#define EXEC_UNIT_OPER_INT_ADD_CY   2
#define EXEC_UNIT_OPER_INT_MUL_CY   5
#define EXEC_UNIT_OPER_LOGIC_CY     2
#define EXEC_UNIT_OPER_FLOAT_ADD_CY 5
#define EXEC_UNIT_OPER_FLOAT_MUL_CY 5
#define EXEC_UNIT_OPER_LS_CY        4 //LOAD/STORE is actually measured in real access

uint64_t EX_PREVIOUS_CY;
uint64_t EX_TOTAL_UNIT_CYCLES[NUMBER_OF_RVU_UNITS];
uint64_t EX_UNIT_INST[NUMBER_OF_RVU_UNITS];
uint64_t EX_UNIT_LS_INST[NUMBER_OF_RVU_UNITS];
uint64_t EX_UNIT_INT_LOGIC_ARITH_INST[NUMBER_OF_RVU_UNITS];
uint64_t EX_UNIT_FLOAT_INST[NUMBER_OF_RVU_UNITS];


/*-----------------------------------END---------------------------------------*/



#endif