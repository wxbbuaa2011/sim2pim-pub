#################### Sim²PIM ####################

@Simple Simulator for PIM architectures. Uses performance counters to acquire precise metrics from the Host processors about the code natively executed.

@This is the preliminary (simplified) public version of Sim²PIM. It will be update with more PIM instructions, examples and information as soon as possible.


####### How do I get set up? #######

# For Intel Skylake processors #
*** Allow RDPMC performance counters
* make in /utils//kernelRDPMC
* $ sudo setup.sh in /utils/scripts

# For an AMD Ryzen processor #
*** for kernel 5.3, should not be necessary any third part installation, in theory, some HPC are natively available and ready to go

# General utils on Hardware Performance Counters that the user may want to use #
* $ sudo sh -c "echo -1 > /proc/sys/kernel/perf_event_paranoid"
* OR
* $ sudo sysctl -w kernel.perf_event_paranoid=-1

* $ sudo sh -c "echo '2' > /sys/bus/event_source/devices/cpu/rdpmc" 


####### Running pre-compiled examples available on /sim2pim/benchmark/src #######
* edit makefile in /sim2pim 
* change the APPOBJ variable
* $ make
* $ ./sim2pim #counter_id (e.g. '0' for cycles)

####### Compiling an application #######
* get PRIMO compiler available on www.pim.computer
* follow its instructions to install
* write the code in C
*** compile the code
* put the c code into the folder /sim2pim/benchmark
* $ sh createASM.sh #benchmark #RVU_vector_size in /sim2pim/utils/scripts
* check your ASM code in /sim2pim/benchmark/src
* edit makefile in /sim2pim
* change the APPOBJ @variable
* $ make
* $ ./sim2pim counter_id ('0' for cycles)

