#ifndef RDPMC
#define RDPMC

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <inttypes.h>
#include <sched.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <getopt.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <cpuid.h>

#include "common.h"

#define IS_INTEL

/*/////////////////////  THESE ADDRESS WILL BE USED VIA WRMSR  ////////////////////////*/
#ifdef IS_AMD
/*=== AMD MSR CONFIG AND ACCESS ADDRESSES ===*/
#define MSR0_CFG  0xC0010200
#define MSR0_READ 0xC0010201 //OR RDPMC 0
#define MSR1_CFG  0xC0010202
#define MSR1_READ 0xC0010203 //OR RDPMC 1
#define MSR2_CFG  0xC0010204
#define MSR2_READ 0xC0010205 //OR RDPMC 2
#define MSR3_CFG  0xC0010206
#define MSR3_READ 0xC0010207 //OR RDPMC 3
#define MSR4_CFG  0xC0010208
#define MSR4_READ 0xC0010209 //OR RDPMC 4
#define MSR5_CFG  0xC001020A
#define MSR5_READ 0xC001020B //OR RDPMC 5

////  EVENTS AMD  ////
#define CYCLES_NOT_IN_HALT    0x0076 //RDPMC 0 = Cycles not in Halt   - PMC LsNotHaltedCyc
#define DATA_CACHE_ACCESS     0x0040 //RDPMC 3 = Data Cache Accesses  - PMC LsDcAccesses
#define RETIRED_INSTRUCTIONS  0x00c0 //RDPMC 4 = Retired Instructions - PMC ExRetInstr

#endif
///////////////////////////////////////////////////////////////////////////////
#ifdef IS_INTEL

#ifndef MSR_IA32_PMC0
#define MSR_IA32_PMC0               0x0C1
#endif
#ifndef MSR_IA32_PERFEVTSEL0
#define MSR_IA32_PERFEVTSEL0        0x186
#endif
#ifndef MSR_OFFCORE_RSP0
#define MSR_OFFCORE_RSP0            0x1A6
#endif
#ifndef MSR_OFFCORE_RSP1
#define MSR_OFFCORE_RSP1            0x1A7
#endif
#ifndef MSR_IA32_FIXED_CTR0
#define MSR_IA32_FIXED_CTR0         0x309
#endif
#ifndef MSR_IA32_FIXED_CTR_CTRL
#define MSR_IA32_FIXED_CTR_CTRL     0x38D
#endif
#ifndef MSR_IA32_PERF_GLOBAL_CTRL
#define MSR_IA32_PERF_GLOBAL_CTRL   0x38F
#endif
#ifndef MSR_PEBS_FRONTEND
#define MSR_PEBS_FRONTEND           0x3F7
#endif
#ifndef CORE_X86_MSR_PERF_CTL
#define CORE_X86_MSR_PERF_CTL       0xC0010200
#endif
#ifndef CORE_X86_MSR_PERF_CTR
#define CORE_X86_MSR_PERF_CTR       0xC0010201
#endif

#endif

#define force_inline __attribute__((always_inline))

#define print_error(...) fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n");
#define print_verbose(...) if (verbose) printf(__VA_ARGS__);
#define print_user_verbose(...) if (verbose) printf(__VA_ARGS__);
#define nb_strtoul(s, base, res) *res = strtoul(s, NULL, base);

extern Config Conf;
extern Counters *Count;

/** READ RDPMC **/
static inline force_inline uint64_t p_rdpmc(uint64_t in){
	uint32_t d, a;

	__asm__ volatile("rdpmc" : "=d" (d), "=a" (a) : "c" (in) : "memory");
	return ((uint64_t)d << 32) | a;
}

/** FORCE SYNC **/
static inline force_inline void RDPMCSync(void){
	__asm__ volatile("mfence" ::: "memory");
}

static inline force_inline void RDPMCSync(void);

static inline force_inline void RDPMCStart(char core){
	__asm__ volatile("mfence" ::: "memory");
  if (Conf.enCounter) Count[core].resume = p_rdpmc(Conf.counter);
  __asm__ volatile("mfence" ::: "memory");
}

static inline force_inline void RDPMCStop(char core){
	__asm__ volatile("mfence" ::: "memory");
  if (Conf.enCounter) Count[core].stop = p_rdpmc(Conf.counter);
  __asm__ volatile("mfence" ::: "memory");
}

static inline force_inline void RDPMCStop_tmp(uint64_t *stop_t){
	__asm__ volatile("mfence" ::: "memory");
  if (Conf.enCounter) *stop_t = p_rdpmc(Conf.counter);
  __asm__ volatile("mfence" ::: "memory");
}

static inline force_inline void RDPMCAccum_tmp(uint64_t *stop_t, int core){
  if (Conf.enCounter)
  {
    Count[core].stop = *stop_t;
    Count[core].accum += Count[core].stop - Count[core].resume;
  }
}

static inline force_inline void RDPMCAccum(char core){
  if (Conf.enCounter) Count[core].accum += Count[core].stop - Count[core].resume;
}

// static inline force_inline void RDPMCStart(char core);
// static inline force_inline void RDPMCStop(char core);
// static inline force_inline void RDPMCStop_tmp(uint64_t *stop_t);
// static inline force_inline void RDPMCAccum(char core);

struct pfc_config {
    unsigned long evt_num;
    unsigned long umask;
    unsigned long cmask;
    unsigned int any;
    unsigned int edge;
    unsigned int inv;
    unsigned long msr_3f6h;
    unsigned long msr_pf;
    unsigned long msr_rsp0;
    unsigned long msr_rsp1;
    unsigned int invalid;
    char* description;
};

extern struct pfc_config pfc_configs[];

void __attribute__ ((noinline)) RDPMCWarmup_call();
void RDPMCWarmup(void);
void RDPMCInit_Prog_Counters(void);
int RDPMCConfig_Prog_Counters(int usr, int os, int cpu, int cntr_type);
uint64_t RDPMCRead_Cmd(char* cmd);
uint64_t RDPMCRead(unsigned int msr, int cpu) ;
void RDPMCWrite(unsigned int msr, uint64_t value, int cpu);
int RDPMCConfing_Fixed_Counters(int usr, int os, int cpu);
void RDPMCPrint(const char *fmt, ...);

#endif /* end of include guard:  */
