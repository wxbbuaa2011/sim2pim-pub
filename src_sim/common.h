#ifndef COMMON
#define COMMON

#include <inttypes.h>

//void vecsum(void);

typedef struct counters {
  uint64_t stop;
  uint64_t resume;
  uint64_t accum;
  char occup;
  char physical_core;
  char core_index;
}Counters;

typedef struct config{
  uint64_t counter;
  char enCounter;
  char isIntel;
  int numProc;
  int usr;
  int os;
}Config;

#endif /* end of include guard:  */
