#ifndef WRAPPER
#define WRAPPER

#define _GNU_SOURCE

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <x86intrin.h>

#include "rdpmc.h"
#include "config.h"

extern volatile uint64_t **pim_FIFO_Addr;

char last_counted;

typedef struct thread_args {
  void *arg;
  void *(*start_routine)(void *);
  int core;
} thread_arg;

int WrapperCreate(pthread_t *thread, const pthread_attr_t *attr,
                          void *(*start_routine) (void *), void *arg);
int WrapperJoin(pthread_t thread, void **retval);
void *WrapperThread(void *args);

#endif
