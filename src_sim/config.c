#include "config.h"

void ConfigHelp(void){

  printf("Pass a valid counter! \n");

  if (Conf.isIntel)
  {
    printf(  "Fixed Counters:\n"
            " 0 --> Instructions retired\n"
            " 1 --> Actual Cycles\n"
            " 2 --> Reference Cycles\n\n"
            "Programmable Counters:\n");
    for (int i = 0; i < 18; i++)
    {
      printf(" %d --> %s\n", i+3, pfc_configs[i].description);
    }
  }else
  {
    printf(  "Fixed Counters:\n"
            " 0 --> asad\n"
            " 3 --> asdasd\n"
            " 4 --> asdasd\n\n");
  }

  printf("Exiting\n\n");

  return;
}

char ConfigToggle_Core(char logical_core){
	Count[logical_core].occup = !Count[logical_core].occup;
	return logical_core;
}

char ConfigGetCoreIndex(char physical_core){

  for (size_t i = 0; i < Conf.numProc; i++) {
    if (Count[i].physical_core == physical_core) {
      return Count[i].core_index;
    }
  }
  printf("ERROR: No CORE INDEX!!\n");
  exit(-1);
}

void ConfigSet_instrumentation(char p_core)
{
  if (Conf.enCounter)
  {
    if (Conf.isIntel)
    {
      RDPMCInit_Prog_Counters();
    }

  // Configure Fixed counters for each core

    if(Conf.isIntel)
    {
      if (Conf.counter > 2)
      {
        for (size_t i = 0; i < Conf.numProc; i++)
        {
          RDPMCConfig_Prog_Counters(Conf.usr, Conf.os, i, Conf.counter-3);
        }
        printf("Programmable Counter %ld - %s\n", Conf.counter,pfc_configs[Conf.counter-3].description);
      } else
      {
        printf("Fixed Counter %ld in CPU %d\n", (Conf.counter),p_core);
        Conf.counter  = (1UL<<30)+Conf.counter;
        for (size_t i = 0; i < Conf.numProc; i++)
        {
          RDPMCConfing_Fixed_Counters(Conf.usr, Conf.os, i);
        }
      }
    } else
    {//AMD
      //Do AMD Config
      // for (size_t i = 0; i < Conf.numProc; i++) {
      //   configure_AMD_cntr(Conf.usr, Conf.os, i*2);
      // }
      // printf("Programmable Counter %ld in CPU %d\n", Conf.counter, p_core);
    }
  }

}

void ConfigCPU_info(void)
{
  printf("\n");
  FILE *cpuinfo = fopen("/proc/cpuinfo","rb");      // opening /proc/cpuinfo to read the cpu details
  char line[1024];
  int count = 0 ;
  // int numProc = sysconf(_SC_NPROCESSORS_ONLN);

  while(fgets(line,1024,cpuinfo) != NULL)
  {
    if(strstr(line,"model name") != NULL)
    {
      Conf.isIntel = strstr(line, "Intel") != NULL ? 1 : 0;
    }
    if (strstr(line,"cpu cores") != NULL)
    {
      sscanf(line,"cpu cores  : %d",&Conf.numProc);
      break;
    }
  }

  Count = (Counters*) malloc(sizeof(Counters)*Conf.numProc);

  FIFO_flag = (atomic_flag*) malloc(sizeof(atomic_flag)*Conf.numProc);
  PIM_store_flag = (atomic_flag*) malloc(sizeof(atomic_flag)*Conf.numProc);
  PIM_start_flag = (atomic_flag*) malloc(sizeof(atomic_flag));
  pim_FIFO_Addr = malloc(sizeof(uint64_t)*Conf.numProc);

  char free_core = 0;
  //This is to ignore the PIM Core, as it does not count for the metrics
  for (size_t i = 0; i < Conf.numProc; i++) {

    Count[i].physical_core = i;

    if (i == BENCH_CORE) {
      Count[i].core_index = 0;
    }else if (i == PIM_CORE) {
      Count[i].core_index = -1;
    } else{
      Count[i].core_index = free_core;
      free_core++;
    }

    //PRINT CORE MAPPING
    // printf("Physical == %d\n", Count[i].physical_core);
    // printf("Virtual == %d\n", Count[i].core_index);

  }


  for (size_t i = 0; i < Conf.numProc; i++)
  {
    Count[i].occup = 0;
    Count[i].accum = 0;
    //create instruciton buffer
    pim_FIFO_Addr[i] = (uint64_t*) malloc(sizeof(uint64_t)*MAX_FIFO*4);
  }
  printf(  "################################################\n"
          "CPU %s"
          "Number of processors: %d \n",
          line,
          Conf.numProc
        );

  fclose(cpuinfo);
}
/*
Reads configs from a user set file

For now hardcoded
*/
void ConfigRead(void){
  Conf.enCounter = 1;
  Conf.isIntel = 1;
  Conf.numProc;
  Conf.usr = 1;
  Conf.os = 0;

}
