#include "buffer.h"

int BufferAppend(uint64_t ins0, uint64_t ins1, uint64_t ls, uint64_t cycle, int t_core)
{

  if ((pim_FIFO_Addr[t_core][MAX_FIFO-1] != 0) ||
      (pim_FIFO_Addr[t_core][(MAX_FIFO*2)-1] != 0))
      {
        return 1; //FIFO Full
  }

  for (size_t i = 0; i < MAX_FIFO; i++) {
    if ((pim_FIFO_Addr[t_core][i] == 0) &&
        (pim_FIFO_Addr[t_core][i+MAX_FIFO] == 0))
        {
          pim_FIFO_Addr[t_core][i] = ins0;
          pim_FIFO_Addr[t_core][i+MAX_FIFO] = ins1;
          pim_FIFO_Addr[t_core][i+MAX_FIFO*2] = ls;
          pim_FIFO_Addr[t_core][i+MAX_FIFO*3] = cycle;
          return 0;
    }
  }
  return 0;
}

int BufferConsume(uint64_t* instructionBuffer, uint64_t* LSBuffer, uint64_t* cycleBuffer, int t_core)
{
  //Get first values
  uint64_t ins0, ins1, ls, cycle;

  ins0 = pim_FIFO_Addr[t_core][0];
  ins1 = pim_FIFO_Addr[t_core][MAX_FIFO];
  ls = pim_FIFO_Addr[t_core][MAX_FIFO*2];
  cycle = pim_FIFO_Addr[t_core][MAX_FIFO*3];

  instructionBuffer[t_core] = ins0;
  instructionBuffer[t_core+Conf.numProc] = ins1;
  LSBuffer[t_core] = ls;
  cycleBuffer[t_core] = cycle;

  if ((ins0==0) && (ins1==0)) {//If FIFO empty
    return -1;
  }

  pim_FIFO_Addr[t_core][0] = 0;
  pim_FIFO_Addr[t_core][MAX_FIFO] = 0;
  pim_FIFO_Addr[t_core][MAX_FIFO*2] = 0;
  pim_FIFO_Addr[t_core][MAX_FIFO*3] = 0;

  //Cycle FIFO
  for (size_t i = 1; i < MAX_FIFO; i++) {
    if ((pim_FIFO_Addr[t_core][i] != 0) ||
        (pim_FIFO_Addr[t_core][i+MAX_FIFO] != 0))
        {
          pim_FIFO_Addr[t_core][i-1] = pim_FIFO_Addr[t_core][i];
          pim_FIFO_Addr[t_core][i-1 +MAX_FIFO] = pim_FIFO_Addr[t_core][i+MAX_FIFO];
          pim_FIFO_Addr[t_core][i-1 +(MAX_FIFO*2)] = pim_FIFO_Addr[t_core][i+MAX_FIFO*2];
          pim_FIFO_Addr[t_core][i-1 +(MAX_FIFO*3)] = pim_FIFO_Addr[t_core][i+MAX_FIFO*3];

          pim_FIFO_Addr[t_core][i] = 0;
          pim_FIFO_Addr[t_core][i+MAX_FIFO] = 0;
          pim_FIFO_Addr[t_core][i+MAX_FIFO*2] = 0;
          pim_FIFO_Addr[t_core][i+MAX_FIFO*3] = 0;
    }else return 0; //no more Ins in FIFO
  }
  return 1;
}
