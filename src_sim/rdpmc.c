#include "rdpmc.h"

uint64_t RDPMCRead_Cmd(char* cmd) {
    FILE* fp;
    if(!(fp = popen(cmd, "r"))){
        print_error("Error reading from \"%s\"", cmd);
        return 0;
    }

    char buf[20];
    fgets(buf, sizeof(buf), fp);
    pclose(fp);

    uint64_t val;
    nb_strtoul(buf, 0, &val);
    return val;
}

uint64_t RDPMCRead(unsigned int msr, int cpu) {
  char cmd[50];
  snprintf(cmd, sizeof(cmd), "rdmsr -c -p%d %#x", cpu, msr);
  return RDPMCRead_Cmd(cmd);
}

void RDPMCWrite(unsigned int msr, uint64_t value, int cpu) {
  char cmd[50];
  snprintf(cmd, sizeof(cmd), "wrmsr -p%d %#x %#lx", cpu, msr, value);
  if (system(cmd)) {
      print_error("\"%s\" failed. You may need to disable Secure Boot (see README.md).", cmd);
      exit(1);
  }
}

int RDPMCConfing_Fixed_Counters(int usr, int os, int cpu){
  if (Conf.isIntel) {
    uint64_t global_ctrl = RDPMCRead(MSR_IA32_PERF_GLOBAL_CTRL,cpu);
    global_ctrl |= ((uint64_t)7 << 32) | 15;
    RDPMCWrite(MSR_IA32_PERF_GLOBAL_CTRL, global_ctrl,cpu);

    uint64_t fixed_ctrl = RDPMCRead(MSR_IA32_FIXED_CTR_CTRL,cpu);
    // disable fixed counters
    fixed_ctrl &= ~((1 << 12) - 1);
    RDPMCWrite(MSR_IA32_FIXED_CTR_CTRL, fixed_ctrl,cpu);
    // clear
    for (int i=0; i<3; i++) {
        RDPMCWrite(MSR_IA32_FIXED_CTR0+i, 0,cpu);
    }
    //enable fixed counters
    fixed_ctrl |= (os << 8) | (os << 4) | os;
    fixed_ctrl |= (usr << 9) | (usr << 5) | (usr << 1);
    RDPMCWrite(MSR_IA32_FIXED_CTR_CTRL, fixed_ctrl,cpu);
  }
  return 0;
}

void RDPMCInit_Prog_Counters(void){

  pfc_configs[0].evt_num = 0xe;
  pfc_configs[0].umask = 0x1;
  pfc_configs[0].description = "UOPS_ISSUED.ANY";

  pfc_configs[1].evt_num = 0xb1;
  pfc_configs[1].umask = 0x1;
  pfc_configs[1].description = "UOPS_EXECUTED.THREAD";

  pfc_configs[2].evt_num = 0xa1;
  pfc_configs[2].umask = 0x1;
  pfc_configs[2].description = "UOPS_DISPATCHED_PORT.PORT_0";

  pfc_configs[3].evt_num = 0xa1;
  pfc_configs[3].umask = 0x2;
  pfc_configs[3].description = "UOPS_DISPATCHED_PORT.PORT_1";

  pfc_configs[4].evt_num = 0xa1;
  pfc_configs[4].umask = 0x4;
  pfc_configs[4].description = "UOPS_DISPATCHED_PORT.PORT_2";

  pfc_configs[5].evt_num = 0xa1;
  pfc_configs[5].umask = 0x8;
  pfc_configs[5].description = "UOPS_DISPATCHED_PORT.PORT_3";

  pfc_configs[6].evt_num = 0xa1;
  pfc_configs[6].umask = 0x10;
  pfc_configs[6].description = "UOPS_DISPATCHED_PORT.PORT_4";

  pfc_configs[7].evt_num = 0xa1;
  pfc_configs[7].umask = 0x20;
  pfc_configs[7].description = "UOPS_DISPATCHED_PORT.PORT_5";

  pfc_configs[8].evt_num = 0xa1;
  pfc_configs[8].umask = 0x40;
  pfc_configs[8].description = "UOPS_DISPATCHED_PORT.PORT_6";

  pfc_configs[9].evt_num = 0xa1;
  pfc_configs[9].umask = 0x80;
  pfc_configs[9].description = "UOPS_DISPATCHED_PORT.PORT_7";

  pfc_configs[10].evt_num = 0xc4;
  pfc_configs[10].umask = 0x0;
  pfc_configs[10].description = "BR_INST_RETIRED.ALL_BRANCHES";

  pfc_configs[11].evt_num = 0xc5;
  pfc_configs[11].umask = 0x4;
  pfc_configs[11].description = "BR_MISP_RETIRED.ALL_BRANCHES";

  pfc_configs[12].evt_num = 0xd1;
  pfc_configs[12].umask = 0x1;
  pfc_configs[12].description = "MEM_LOAD_RETIRED.L1_HIT";

  pfc_configs[13].evt_num = 0xd1;
  pfc_configs[13].umask = 0x8;
  pfc_configs[13].description = "MEM_LOAD_RETIRED.L1_MISS";

  pfc_configs[14].evt_num = 0xd1;
  pfc_configs[14].umask = 0x2;
  pfc_configs[14].description = "MEM_LOAD_RETIRED.L2_HIT";

  pfc_configs[15].evt_num = 0xd1;
  pfc_configs[15].umask = 0x10;
  pfc_configs[15].description = "MEM_LOAD_RETIRED.L2_MISS";

  pfc_configs[16].evt_num = 0xd1;
  pfc_configs[16].umask = 0x4;
  pfc_configs[16].description = "MEM_LOAD_RETIRED.L3_HIT";

  pfc_configs[17].evt_num = 0xd1;
  pfc_configs[17].umask = 0x20;
  pfc_configs[17].description = "MEM_LOAD_RETIRED.L3_MISS";

  for (size_t i = 0; i < 18; i++) {
    pfc_configs[i].cmask = 0;
    pfc_configs[i].any = 0;
    pfc_configs[i].edge = 0;
    pfc_configs[i].inv = 0;
    pfc_configs[i].msr_3f6h = 0;
    pfc_configs[i].msr_pf = 0;
    pfc_configs[i].msr_rsp0 = 0;
    pfc_configs[i].msr_rsp1 = 0;
    pfc_configs[i].invalid = 0;
  }

}

int RDPMCConfig_Prog_Counters(int usr, int os, int cpu, int cntr_type){
  if (Conf.isIntel) {
      uint64_t global_ctrl = RDPMCRead(MSR_IA32_PERF_GLOBAL_CTRL,cpu);
      global_ctrl |= ((uint64_t)7 << 32) | 15;
      RDPMCWrite(MSR_IA32_PERF_GLOBAL_CTRL, global_ctrl,cpu);

      uint64_t perfevtselx = RDPMCRead(MSR_IA32_PERFEVTSEL0,cpu);

      // disable counter
      perfevtselx &= ~(((uint64_t)1 << 32) - 1);
      RDPMCWrite(MSR_IA32_PERFEVTSEL0, perfevtselx,cpu);

      // clear
      RDPMCWrite(MSR_IA32_PMC0, 0,cpu);

      // configure counter
      struct pfc_config config = pfc_configs[cntr_type];
      if (config.invalid) {
          return -1;
      }
      perfevtselx |= ((config.cmask & 0xFF) << 24);
      perfevtselx |= (config.inv << 23);
      perfevtselx |= (1ULL << 22);
      perfevtselx |= (config.any << 21);
      perfevtselx |= (config.edge << 18);
      perfevtselx |= (os << 17);
      perfevtselx |= (usr << 16);
      perfevtselx |= ((config.umask & 0xFF) << 8);
      perfevtselx |= (config.evt_num & 0xFF);
      RDPMCWrite(MSR_IA32_PERFEVTSEL0, perfevtselx,cpu);

      if (config.msr_3f6h) {
          RDPMCWrite(0x3f6, config.msr_3f6h,cpu);
      }

      if (config.msr_pf) {
          RDPMCWrite(MSR_PEBS_FRONTEND, config.msr_pf,cpu);
      }

      if (config.msr_rsp0) {
          RDPMCWrite(MSR_OFFCORE_RSP0, config.msr_rsp0,cpu);
      }
      if (config.msr_rsp1) {
          RDPMCWrite(MSR_OFFCORE_RSP1, config.msr_rsp1,cpu);
      }
  } else {
      // clear
      RDPMCWrite(CORE_X86_MSR_PERF_CTR, 0,cpu);

      struct pfc_config config = pfc_configs[cntr_type];

      uint64_t perf_ctl = 0;
      perf_ctl |= ((config.evt_num) & 0xF00) << 24;
      perf_ctl |= (config.evt_num) & 0xFF;
      perf_ctl |= ((config.umask) & 0xFF) << 8;
      perf_ctl |= ((config.cmask) & 0x7F) << 24;
      perf_ctl |= (config.inv << 23);
      perf_ctl |= (1ULL << 22);
      perf_ctl |= (config.edge << 18);
      perf_ctl |= (os << 17);
      perf_ctl |= (usr << 16);

      RDPMCWrite(CORE_X86_MSR_PERF_CTL, perf_ctl,cpu);
  }
}

void __attribute__ ((noinline)) RDPMCWarmup_call(){
  //PARTIAL MEASURE
  __asm__ volatile("mfence" ::: "memory");
  Count[0].stop = p_rdpmc(Conf.counter);
  __asm__ volatile("mfence" ::: "memory");

  Count[0].accum += Count[0].stop - Count[0].resume;
  // printf("%ld - %ld = %ld -> Accum = %ld\n",stop, resume,(stop - resume),accumCounter);
}

void RDPMCPrint(const char *fmt, ...){
  if (Conf.enCounter) {
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
  }
}

void RDPMCWarmup(void){//MUST REDO FOR ALL CORES!
  if (Conf.enCounter) {
    //backtoback overhead:
    int repetition = 10000;
    Count[0].accum = 0;

    uint64_t overheadBB = Count[0].accum/repetition;

    for (size_t i = 0; i < repetition; i++) {
      __asm__ volatile("mfence" ::: "memory");
      Count[0].resume = p_rdpmc(Conf.counter);
      __asm__ volatile("mfence" ::: "memory");
      __asm__ volatile("mfence" ::: "memory");
      Count[0].stop = p_rdpmc(Conf.counter);
      __asm__ volatile("mfence" ::: "memory");
      Count[0].accum += Count[0].stop - Count[0].resume;
    }


    ///Function call overhead:
    Count[0].accum = 0;
    for (size_t i = 0; i < repetition; i++) {
      __asm__ volatile("mfence" ::: "memory");
      Count[0].resume = p_rdpmc(Conf.counter);
      __asm__ volatile("mfence" ::: "memory");
      //=== benchmark + simulation ===//
      RDPMCWarmup_call();
    }

    printf( "################################################\n"
            "RDPMCWarmup Results:\n"
            "  Overhead Back to Back = %lu\n"
            "  Overhead Func Call = %lu\n"
            "################################################\n\n",
            overheadBB,
            Count[0].accum/repetition
          );
  }
}
