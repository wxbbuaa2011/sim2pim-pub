#ifndef SIM_CONFIGURE
#define SIM_CONFIGURE

#include "rdpmc.h"
// struct pfc_config pfc_configs[18] = {{0}};
struct pfc_config pfc_configs[18];
/** SIMULATION TEST ENVIRONMENT **/
// #define PIM_ADDR 0x0
#define BENCH_CORE 0
#define PIM_CORE 1
#define MAX_THREAD 2
#define MAX_FIFO 32

int is_Intel_CPU;
int cpu_sim;
int cpu_bench;
int usr;
int os;

#endif /* end of include guard:  */
